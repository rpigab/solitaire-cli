#[macro_use]
extern crate failure_derive;

pub(crate) mod board;
pub mod card;
pub mod error;
pub mod selection;
pub(crate) mod move_checks;
pub mod action;
pub mod model;
pub mod card_stack;
pub(crate) mod random;
pub mod replay;
pub mod game_over;
pub mod game_options;
pub mod new_game_mode;
