use std::fmt::{Display, Formatter};

use serde::Deserialize;
use serde::Serialize;

use crate::action::intent::Intent;
use crate::action::undo_record::{UndoElement, UndoRecord};
use crate::error::GameError::TechnicalErrorMessage;
use crate::error::Result;
use crate::game_options::DrawMode;

#[derive(Serialize, Deserialize)]
pub struct Replay {
    pub(crate) seed: String,
    pub(crate) move_intents: Vec<Intent>,
    pub draw_mode: DrawMode,
}

impl Replay {
    pub(crate) fn new(seed: String, undo_record: &UndoRecord, draw_mode: DrawMode) -> Self {
        Self {
            seed,
            move_intents: {
                undo_record.actions()
                    .iter()
                    .map(UndoElement::intent)
                    .map(Clone::clone)
                    .collect()
            },
            draw_mode,
        }
    }

    pub fn to_cbor_base64(&self) -> Result<String> {
        let replay_cbor = serde_cbor::to_vec(self)
            .map_err(|_| TechnicalErrorMessage { message: "error serializing replay to cbor".to_string() })?;
        Ok(base64::encode(replay_cbor))
    }

    pub fn from_cbor_base64(s: String) -> Result<Self> {
        let replay_cbor = base64::decode(s)
            .map_err(|_| TechnicalErrorMessage { message: "error deserializing replay from b64".to_string() })?;
        let replay: Replay = serde_cbor::from_slice(&*replay_cbor)
            .map_err(|_| TechnicalErrorMessage { message: "error deserializing replay from cbor".to_string() })?;
        Ok(replay)
    }

    pub fn from_string(s: String) -> Result<Self> {
        serde_json::from_str::<Replay>(&s)
            .map_err(|_| TechnicalErrorMessage { message: "error deserializing from replay string".to_string() })
    }

    pub fn to_string(&self) -> Result<String> {
        serde_json::to_string(self)
            .map_err(|_| TechnicalErrorMessage { message: "error serializing replay to string".into() })
    }
}


impl Display for Replay {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "moves len: {}, first: {:?}", self.move_intents.len(), self.move_intents.first())
    }
}