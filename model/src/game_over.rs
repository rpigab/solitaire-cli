pub enum GameOver {
    /// Game is not over
    No,
    /// When there are no hidden cards, and deck is empty
    SecondaryVictory,
    /// Game is won and no more moves can be made
    PrimaryVictory,
}
