use bytes::Bytes;
use rand::Rng;
use rand_chacha::{ChaCha20Rng, ChaChaRng};
use rand_chacha::rand_core::SeedableRng;
use serde::{Serialize, Serializer};

use crate::error::GameError::{TechnicalError, TechnicalErrorMessage};
use crate::error::Result;

/// A seed and rng
pub(crate) struct Gen {
    seed: Bytes,
    rng: ChaCha20Rng,
}

impl Gen {

    pub(crate) fn make_gen(seed: Option<String>) -> Result<Self> {
        let gen = match seed {
            seed @ Some(..) => {
                log::info!("gen: seed loaded from replay");
                Gen::new_fixed_seed(seed)
            }
            None if cfg!(feature = "fixedseed") => {
                log::info!("gen: fixed seed");
                Gen::new_fixed_seed(None)
            }
            _ => {
                log::info!("gen: random seed");
                Ok(Gen::new_random_seed())
            }
        }?;
        log::info!("Seed hex: {}", gen.get_seed_hex());
        Ok(gen)
    }

    fn new_fixed_seed(seed: Option<String>) -> Result<Self> {
        const SEED: [u8; 32] = [42_u8; 32];
        let rng = match seed {
            None => ChaChaRng::from_seed(SEED),
            Some(seed) => {
                let seed = get_seed_from_hex_string(seed)?.to_vec();
                let seed: [u8; 32] = seed.try_into()
                    .map_err(|_| TechnicalErrorMessage {
                        message: "error decoding seed from hex string".to_string()
                    })?;
                ChaChaRng::from_seed(seed)
            }
        };
        Ok(Self { seed: Bytes::copy_from_slice(&SEED), rng })
    }

    pub(crate) fn gen_from_seed_bytes(seed: [u8; 32]) -> Self {
        let rng = ChaChaRng::from_seed(seed);
        Self { seed: Bytes::copy_from_slice(&seed), rng }
    }

    fn new_random_seed() -> Self {
        let mut rng = ChaChaRng::from_entropy();
        let mut random_seed: [u8; 32] = Default::default();
        rng.fill(&mut random_seed);

        let rng = ChaChaRng::from_seed(random_seed);
        Self { seed: Bytes::copy_from_slice(&random_seed), rng }
    }

    pub(crate) fn get_seed_hex(&self) -> String {
        let mut s = String::new();
        for byte in &self.seed {
            s += format!("{:02x}", byte).as_ref();
        }
        s
    }

    pub(crate) fn rng_mut(&mut self) -> &mut ChaCha20Rng {
        &mut self.rng
    }

    pub fn seed(&self) -> Result<[u8; 32]> {
        let res: [u8; 32] = *&self.seed.to_vec().try_into()
            .map_err(|_| TechnicalError)?;
        Ok(res)
    }
}

fn get_seed_from_hex_string(seed: String) -> Result<Bytes> {
    let bytes = hex::decode(&seed)
        .map_err(|_| TechnicalErrorMessage {
            message: format!("error decing seed from hex string {}", seed)
        })?;
    Ok(Bytes::copy_from_slice(&bytes))
}

impl Serialize for Gen {
    fn serialize<S>(&self, serializer: S) -> core::result::Result<S::Ok, S::Error>
        where
            S: Serializer,
    {
        serializer.serialize_bytes(&self.seed)
    }
}

#[cfg(test)]
mod tests {
    use bytes::Bytes;

    use crate::random::gen::get_seed_from_hex_string;

    #[test]
    fn get_seed_from_hex_string_ok() {
        let seed_hex = "2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a2a".to_string();
        let res = get_seed_from_hex_string(seed_hex).expect("error");
        const EXPECTED: [u8; 32] = [42_u8; 32];
        assert_eq!(res, Bytes::from_static(&EXPECTED));
    }
}
