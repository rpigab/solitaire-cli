use crate::card::card::Card;
use crate::error::GameError::{IllegalAction, TechnicalErrorMessage};
use crate::error::Result;
use crate::game_options::DrawMode;
use crate::move_checks::CardStack;

#[derive(Debug)]
pub struct Waste {
    cards: Vec<Card>,
}

impl Waste {
    pub(crate) fn new() -> Waste {
        Waste {
            cards: Vec::new(),
        }
    }

    pub fn get_visible_cards(&self, draw_mode: &DrawMode) -> &[Card] {
        let max_visible_cards = draw_mode.get_num_cards_to_draw();
        if self.cards.len() < max_visible_cards as usize {
            return &self.cards;
        }
        let idx = self.cards.len() - max_visible_cards as usize;
        &self.cards[idx..]
    }

    pub(crate) fn is_empty(&self) -> bool {
        self.cards.is_empty()
    }

    pub(crate) fn put_card(&mut self, card: Card) -> Result<()> {
        Ok(self.cards.push(card))
    }

    /// Put multiple cards from deck to waste.
    pub(crate) fn put_cards(&mut self, cards: Vec<Card>) {
        cards.into_iter()
            .for_each(|card| self.cards.push(card));
    }

    pub(crate) fn drain(&mut self) -> Vec<Card> {
        self.cards.drain(..).collect()
    }

    pub(crate) fn take_top_card(&mut self) -> Result<(Card, bool)> {
        let res = self.cards.pop().ok_or(IllegalAction)?;
        Ok((res, false))
    }

    /// Take cards from end of vec
    pub(crate) fn undo_draw(&mut self, cards_drawn: u8) -> Vec<Card> {
        let idx = self.cards.len() - cards_drawn as usize;
        self.cards.drain(idx..).collect()
    }

    pub(crate) fn fill(&mut self, cards: Vec<Card>) {
        self.cards.extend(cards);
    }
}

impl CardStack for &Waste {
    fn peek_top_card(&self) -> Result<&Card> {
        self.cards.last().ok_or(IllegalAction)
    }

    fn can_put_card(&self, _card: &Card) -> Result<bool> {
        Err(TechnicalErrorMessage { message: "Can't put cards on waste".to_string() })
    }
}
