use crate::card::card::Card;
use crate::card::value::Value;
use crate::error::GameError::{IllegalAction, TechnicalError};
use crate::error::Result;
use crate::move_checks::CardStack;

#[derive(Debug, Default)]
pub struct Pile {
    cards: Vec<Card>,
    hidden_cards: usize,
}

impl Pile {
    pub(crate) fn new() -> Pile {
        Pile {
            cards: vec![],
            hidden_cards: 0,
        }
    }

    pub(crate) fn has_visible_cards(&self) -> bool {
        self.get_size_visible() != 0
    }

    pub(crate) fn has_hidden_cards(&self) -> bool {
        self.hidden_cards != 0
    }

    pub(crate) fn size(&self) -> usize {
        self.cards.len()
    }

    pub(crate) fn get_size_visible(&self) -> usize {
        self.cards.len() - self.hidden_cards
    }

    pub(crate) fn get_first_visible_card(&self) -> Option<&Card> {
        if self.get_size_visible() < 1 {
            None
        } else {
            self.cards.last()
        }
    }

    pub(crate) fn get_main_selected_card(&self, num_cards_selected: usize) -> &Card {
        if self.get_size_visible() < num_cards_selected {
            panic!("more selected cards than visible in pile");
        }
        self.cards.get(self.cards.len() - num_cards_selected).unwrap()
    }

    pub(crate) fn get_nth_visible_card(&self, number: usize) -> Result<&Card> {
        if number > self.cards.len() {
            return Err(TechnicalError);
        }
        let idx = self.cards.len() - number;
        if idx < self.hidden_cards {
            return Err(TechnicalError);
        }
        self.cards.get(idx).ok_or(TechnicalError)
    }

    pub(crate) fn check_can_put(&self, card: &Card) -> Result<bool> {
        let top = self.cards.last();
        let res = (top.is_none() && card.value() == Value::King)
            || (top.is_some() && self.hidden_cards < self.cards.len()
            && top.unwrap().value().follows(card.value())
            && top.unwrap().suit().get_color() != card.suit().get_color());
        Ok(res)
    }

    pub(crate) fn put_cards(&mut self, cards: Vec<Card>) -> Result<()> {
        self.cards.extend(cards);
        Ok(())
    }

    fn reveal_last_card(&mut self) -> bool {
        if self.hidden_cards == self.cards.len()
            && self.cards.len() > 0 {
            self.hidden_cards -= 1;
            return true;
        }
        return false;
    }

    pub(crate) fn hide_last_visible_card(&mut self) -> Result<()> {
        if !self.has_visible_cards() {
            return Err(TechnicalError);
        }
        self.hidden_cards += 1;
        Ok(())
    }

    /// Take N cards from pile, revealing last hidden card if let exposed.
    pub(crate) fn take_stack(&mut self, number: usize) -> Result<(Vec<Card>, bool)> {
        if number > self.cards.len() - self.hidden_cards {
            return Err(IllegalAction);
        }
        let res = self.cards.drain(self.cards.len() - number..)
            .collect();
        Ok((res, self.reveal_last_card()))
    }

    /// View pile, in a tuple containing number of hidden cards and a vector of visible cards.
    pub fn view_pile(&self) -> (usize, &[Card]) {
        (self.hidden_cards, &self.cards[self.hidden_cards..])
    }

    pub(crate) fn init(&mut self, cards: Vec<Card>) {
        self.cards.extend(cards);
        // Reveal card on top of pile
        self.hidden_cards = self.cards.len() - 1;
    }

    pub(crate) fn take_top_card(&mut self) -> Result<(Card, bool)> {
        let res = self.cards.pop().ok_or(IllegalAction)?;
        Ok((res, self.reveal_last_card()))
    }

    pub(crate) fn put_card(&mut self, card: Card) -> Result<()> {
        self.cards.push(card);
        Ok(())
    }
}

impl CardStack for &Pile {
    fn peek_top_card(&self) -> Result<&Card> {
        self.cards.last().ok_or(IllegalAction)
    }

    fn can_put_card(&self, card: &Card) -> Result<bool> {
        self.check_can_put(card)
    }
}


#[cfg(test)]
mod tests {
    use crate::card::card::Card;
    use crate::card::suit::Suit::{Clubs, Diamonds, Hearts};
    use crate::card::value::Value::{Eight, King, Nine, Ten, Three};
    use crate::card_stack::pile::Pile;

    #[test]
    fn view_pile_ok() {
        let mut pile = Pile::new();
        pile.init(vec![
            Card::new(Three, Clubs),
            Card::new(King, Diamonds),
            Card::new(Ten, Hearts),
            Card::new(Nine, Clubs),
        ]);
        println!("{:?}", pile);

        let (hidden_cards, visible_cards) = pile.view_pile();
        assert_eq!(3, hidden_cards);
        assert_eq!(1, visible_cards.len());
        assert_eq!(Card::new(Nine, Clubs), visible_cards[0]);
    }

    #[test]
    fn take_stack_ok() {
        let mut pile = Pile::new();
        pile.init(vec![
            Card::new(Three, Clubs),
            Card::new(King, Diamonds),
            Card::new(Ten, Hearts),
            Card::new(Nine, Clubs),
        ]);
        pile.put_cards(vec![Card::new(Eight, Hearts)]).unwrap();
        println!("{:#?}", pile);

        let (res, revealed) = pile.take_stack(2).unwrap();

        println!("{:#?}", pile);
        assert_eq!(2, pile.hidden_cards);
        assert_eq!(3, pile.cards.len());
        assert_eq!(2, res.len());
        assert_eq!(true, revealed);
    }
}

