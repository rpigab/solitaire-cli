use std::cmp::min;

use log;
use rand::seq::SliceRandom;
use rand_chacha::ChaCha20Rng;

use crate::card::card::Card;
use crate::card::suit::ALL_SUITS;
use crate::card::value::ALL_VALUES;
use crate::random::gen::Gen;

#[derive(Debug)]
pub struct Deck {
    cards: Vec<Card>,
}

impl Deck {
    pub(crate) fn new() -> Deck {
        Deck {
            cards: Vec::new(),
        }
    }

    /// Create a full stack of all cards and shuffle it.
    pub(crate) fn init(&mut self, gen: &mut Gen) {
        self.cards.extend(ALL_SUITS.iter()
            .map(|&s|
                ALL_VALUES.iter()
                    .map(|&v| Card::new(v, s))
                    .collect::<Vec<Card>>()
            ).flat_map(|cards| cards.into_iter()));
        self.shuffle(gen.rng_mut());
    }

    /// Shuffles the deck in-place.
    fn shuffle(&mut self, rng: &mut ChaCha20Rng) {
        self.cards.shuffle(rng)
    }

    /// Take `number_of_cards` from deck, returning drawn Cards in a Vec
    pub(crate) fn draw_cards(&mut self, number_of_cards: u8) -> Vec<Card> {
        if self.cards.is_empty() {
            vec![]
        } else {
            let cards_to_draw = min(self.cards.len(), number_of_cards as usize);
            self.cards.drain(0..cards_to_draw).collect()
        }
    }

    /// Put cards at beginning of vec
    pub(crate) fn put_cards(&mut self, mut cards: Vec<Card>) {
        log::debug!("{:?}", self.cards);
        cards.append(&mut self.cards);
        self.cards = cards;
        log::debug!("{:?}", self.cards);
    }

    pub fn is_empty(&self) -> bool {
        self.cards.is_empty()
    }

    pub(crate) fn fill(&mut self, cards: Vec<Card>) {
        self.cards.extend(cards);
    }

    pub(crate) fn drain(&mut self) -> Vec<Card> {
        self.cards.drain(..).collect()
    }
}

#[cfg(test)]
mod tests {
    use rand::prelude::SliceRandom;
    use rand_chacha::ChaChaRng;
    use rand_chacha::rand_core::SeedableRng;

    use crate::card::card::Card;
    use crate::card::suit::Suit;
    use crate::card::value::Value;
    use crate::card_stack::deck::Deck;
    use crate::random::gen::Gen;

    #[test]
    fn init_ok() {
        let mut deck = Deck::new();
        assert_eq!(deck.cards.is_empty(), true);
        let mut gen: Gen = Gen::make_gen(None).unwrap();
        deck.init(&mut gen);
        assert_eq!(deck.cards.is_empty(), false);
    }

    #[test]
    fn get_char_ok() {
        let deck = Deck::new();
        assert_eq!(deck.is_empty(), true);
    }

    #[test]
    fn draw_cards_ok() {
        let mut deck = Deck::new();
        deck.cards.push(Card::new(Value::Ace, Suit::Diamonds));
        deck.cards.push(Card::new(Value::Two, Suit::Diamonds));
        deck.cards.push(Card::new(Value::Three, Suit::Diamonds));
        assert_eq!(deck.cards.len(), 3);
        let pick_2 = deck.draw_cards(2);
        assert_eq!(pick_2.len(), 2);
        assert_eq!(deck.cards.len(), 1);
    }

    #[test]
    fn seed_random() {
        let seed = [42; 32];
        let mut rng = ChaChaRng::from_seed(seed);
        let mut v1 = vec![1, 2, 3, 4, 5];
        v1.shuffle(&mut rng);
        assert_eq!(v1, [3, 4, 5, 1, 2]);
    }
}
