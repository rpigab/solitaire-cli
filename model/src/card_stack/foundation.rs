use crate::card::card::Card;
use crate::card::value::Value;
use crate::error::GameError::IllegalAction;
use crate::error::Result;
use crate::move_checks::CardStack;

#[derive(Debug)]
pub struct Foundation {
    cards: Vec<Card>,
}

impl Foundation {
    pub(crate) fn new() -> Foundation {
        Foundation {
            cards: Vec::new(),
        }
    }

    pub(crate) fn is_empty(&self) -> bool {
        self.cards.is_empty()
    }

    pub(crate) fn size(&self) -> usize {
        self.cards.len()
    }

    pub fn get_top_card(&self) -> Option<&Card> {
        self.cards.last()
    }

    pub(crate) fn take_top_card(&mut self) -> Result<(Card, bool)> {
        let res = self.cards.pop().ok_or(IllegalAction)?;
        Ok((res, false))
    }

    pub(crate) fn put_card(&mut self, card: Card) -> Result<()> {
        self.cards.push(card);
        Ok(())
    }
}

impl CardStack for &Foundation {
    fn peek_top_card(&self) -> Result<&Card> {
        self.cards.last().ok_or(IllegalAction)
    }

    fn can_put_card(&self, card: &Card) -> Result<bool> {
        // Can put card on foundation if foundation is empty and it's an ace
        // or non empty, same suit, value directly follows
        let top = self.cards.last();
        let res = (top.is_none() && card.value() == Value::Ace)
            || (top.is_some()
            && top.unwrap().suit() == card.suit()
            && card.value().follows(top.unwrap().value()));
        Ok(res)
    }
}
