use crate::action::card_stack::CardStackRef;
use crate::action::intent::Intent;
use crate::action::redo_record::RedoRecord;
use crate::action::undo_record::UndoRecord;
use crate::board::Board;
use crate::card::value::Value;
use crate::card_stack::pile::Pile;
use crate::error::{GameError, Result};
use crate::error::GameError::{InvalidOperation, TechnicalError, TechnicalErrorMessage};
use crate::game_options::DrawMode;
use crate::game_over::GameOver;
use crate::new_game_mode::NewGameMode;
use crate::random::gen::Gen;
use crate::replay::Replay;
use crate::selection::Selection;

pub struct Model {
    board: Board,
    gen: Gen,
    undo_record: UndoRecord,
    redo_record: RedoRecord,
    draw_mode: DrawMode,
}

impl Model {
    pub fn new(mode: NewGameMode, draw_mode: DrawMode) -> Result<Self> {
        let (redo_record, board, gen, draw_mode) = match mode {
            NewGameMode::Basic => {
                let (board, gen) = Board::new(None)?;
                (RedoRecord::new(), board, gen, draw_mode)
            }
            NewGameMode::WithReplay(Replay { seed, move_intents, draw_mode }) => {
                let (board, gen) = Board::new(Some(seed))?;
                (RedoRecord::from_replay(move_intents.into_iter().rev().collect()), board, gen, draw_mode)
            }
            NewGameMode::WithSeed(seed) => {
                let mut gen = Gen::gen_from_seed_bytes(seed);
                let board = Board::new_with_gen(&mut gen);
                (RedoRecord::new(), board, gen, draw_mode)
            }
        };

        Ok(Self {
            board,
            gen,
            undo_record: UndoRecord::new(),
            redo_record,
            draw_mode,
        })
    }

    // Getters

    pub fn board(&self) -> &Board {
        &self.board
    }

    pub fn board_mut(&mut self) -> &mut Board {
        &mut self.board
    }

    pub fn record(&self) -> &UndoRecord {
        &self.undo_record
    }

    pub fn record_mut(&mut self) -> &mut UndoRecord {
        &mut self.undo_record
    }

    pub fn draw_mode(&self) -> &DrawMode {
        &self.draw_mode
    }

    pub fn seed(&self) -> Result<[u8; 32]> {
        self.gen.seed()
    }


    /// Draw n cards from deck
    pub fn draw(&mut self, cards_to_draw: u8) -> Result<()> {
        let draw_intent = Intent::Draw { cards: cards_to_draw };
        draw_intent.apply_from_intent(&mut self.board, &mut self.undo_record)?;
        self.redo_record.clear();
        Ok(())
    }

    /// Move selection to best valid spot
    pub fn smart_move(&mut self, selection: &Selection) -> Result<()> {
        match selection {
            Selection::Nothing => {}
            Selection::Waste => {
                // Try foundations first
                // let card = &self.board.waste().get_visible_cards(self.draw_mode()).last().unwrap();
                if self.try_move_1_to_foundation(selection).is_ok() {
                    return Ok(());
                }
                let card = self.board.waste().get_visible_cards(self.draw_mode()).last().unwrap();
                // Next, try piles
                let pile = self.board.find_spot_in_any_pile_for_card(card, None)?;
                self.move_1(selection, pile.into())?;
                return Ok(());
            }
            Selection::Foundation(src_foundation_idx) => {
                // Can only move from foundation to piles
                let card = self.board().foundations()
                    .get(*src_foundation_idx).unwrap()
                    .get_top_card().unwrap();
                let pile = self.board.find_spot_in_any_pile_for_card(card, None)?;
                self.move_1(selection, pile.into())?;
                return Ok(());
            }
            Selection::Pile { idx, cards: num_cards_selected } => {
                // If only one card is selected, check foundations then other piles
                // Else if multiple cards are selected, check other piles
                if *num_cards_selected == 1 {
                    // First check foundations
                    if self.try_move_1_to_foundation(selection).is_ok() {
                        return Ok(());
                    }

                    // Then piles
                    let card = self.board().get_first_visible_card_from_pile(*idx)
                        .ok_or(TechnicalError)?;
                    let pile = self.board.find_spot_in_any_pile_for_card(card, Some(*idx))?;
                    self.move_1(selection, pile.into())?;
                    return Ok(());
                } else {
                    let card = self.board().get_main_selected_card_from_pile(*idx, *num_cards_selected);
                    let pile = self.board.find_spot_in_any_pile_for_card(card, Some(*idx))?;
                    if let CardStackRef::Pile(target_pile_idx) = pile {
                        self.move_n_pile_to_pile(*idx, target_pile_idx, *num_cards_selected)?;
                        return Ok(());
                    } else {
                        panic!();
                    }
                }
            }
        }

        Err(GameError::InvalidOperationMessage { message: "No smart move found".to_string() })
    }

    /// If `card` can go to a foundation, move it and return sucess, else return failure
    fn try_move_1_to_foundation(&mut self, selection: &Selection) -> Result<()> {
        let card = if *selection == Selection::Waste {
            self.board.waste().get_visible_cards(self.draw_mode()).last().unwrap()
        } else if let Selection::Pile { idx, cards: 1 } = selection {
            self.board.get_first_visible_card_from_pile(*idx).unwrap()
        } else {
            panic!()
        };
        let foundation = if card.value() == Value::Ace {
            self.board().get_any_available_foundation()
                .map_err(|_| TechnicalErrorMessage {
                    message: "No empty foundation found, but an ace is available".to_string()
                })
        } else {
            // Else, find available foundation for suit, if found try moving it
            self.board().find_foundation_by_suit(card.suit())
        };
        return match foundation {
            Ok(foundation) => {
                self.move_1(selection, foundation.into()).map_or(Err(InvalidOperation), |()| Ok(()))
            }
            Err(_) => { Err(InvalidOperation) }
        };
    }

    pub fn move_n_pile_to_pile(&mut self, source_idx: usize, target_idx: usize, cards: usize)
                               -> Result<()> {
        let move_n_intent = Intent::MoveN {
            card_source: CardStackRef::Pile(source_idx),
            card_target: CardStackRef::Pile(target_idx),
            cards,
        };
        move_n_intent.apply_from_intent(&mut self.board, &mut self.undo_record)?;
        self.redo_record.clear();
        Ok(())
    }

    pub fn move_1(&mut self, selection: &Selection, target: Selection) -> Result<()> {
        let move_1_intent = Intent::move_1_intent_from_selections(selection, target)?;
        move_1_intent.apply_from_intent(&mut self.board, &mut self.undo_record)?;
        self.redo_record.clear();
        Ok(())
    }

    /// Undo last action
    pub fn undo(&mut self) -> Result<()> {
        self.undo_record.undo(&mut self.board, &mut self.redo_record)
    }

    pub fn redo(&mut self) -> Result<()> {
        self.redo_record.redo(&mut self.board, &mut self.undo_record)
    }

    pub fn check_victory(&self) -> GameOver {
        if self.board.foundations().iter().all(|f| f.size() == 13) {
            GameOver::PrimaryVictory
        } else if self.board.deck().is_empty()
            && self.board.waste().is_empty()
            && !self.board.piles().iter().any(Pile::has_hidden_cards) {
            GameOver::SecondaryVictory
        } else {
            GameOver::No
        }
    }

    pub fn get_replay(&self) -> Result<Replay> {
        Ok(Replay::new(self.gen.get_seed_hex(), &self.undo_record, self.draw_mode))
    }

    /// Get number of moves used in this game, which is the size of the Undoable stack
    pub fn get_num_moves(&self) -> usize {
        self.undo_record.size()
    }
}
