use std::fmt::Debug;

use crate::card::card::Card;
use crate::error::Result;

pub(crate) trait CardStack: Debug {
    fn peek_top_card(&self) -> Result<&Card>;

    fn can_put_card(&self, card: &Card) -> Result<bool>;
}
