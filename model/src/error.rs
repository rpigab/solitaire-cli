use std::result;

#[derive(Debug, Fail, PartialEq)]
pub enum GameError {
    /// Invalid operation (e.g. trying to select from empty stack).
    #[fail(display = "Invalid operation")]
    InvalidOperation,

    /// Invalid operation (e.g. trying to select from empty stack).
    #[fail(display = "Invalid operation: {}", message)]
    InvalidOperationMessage { message: String },

    /// Illegal action (because of the rules of the game).
    #[fail(display = "Cannot perform action")]
    IllegalAction,

    /// Technical error (e.g. index out of bounds).
    #[fail(display = "Technical error")]
    TechnicalError,

    /// Technical error (e.g. index out of bounds).
    #[fail(display = "Technical error: {}", message)]
    TechnicalErrorMessage { message: String },
}

pub type Result<T> = result::Result<T, GameError>;
