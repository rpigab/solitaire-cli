use std::fmt::Debug;

use serde::Deserialize;
use serde::Serialize;

use crate::action::action_effect::ActionEffect;
use crate::action::card_stack::CardStackRef;
use crate::action::undo_record::UndoRecord;
use crate::board::Board;
use crate::error::GameError::TechnicalError;
use crate::error::Result;
use crate::selection::Selection;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub(crate) enum Intent {
    /// number of cards to draw
    Draw { cards: u8 },
    Move1 {
        card_source: CardStackRef,
        card_target: CardStackRef,
    },
    MoveN {
        card_source: CardStackRef,
        card_target: CardStackRef,
        /// number of cards to move
        cards: usize,
    },
}


impl Intent {
    pub(crate) fn apply_from_intent(self, board: &mut Board, record: &mut UndoRecord) -> Result<()> {
        let action_effect = match &self {
            Intent::Draw { cards } => {
                let cards_drawn = board.draw(*cards);
                ActionEffect::Draw { cards_drawn }
            }
            Intent::Move1 { card_source, card_target } => {
                board.move_1(&card_source, &card_target)?
            }
            Intent::MoveN { card_source, card_target, cards } => {
                let (source_idx, target_idx) = match (card_source, card_target) {
                    (CardStackRef::Pile(source_idx), CardStackRef::Pile(target_idx)) => {
                        (source_idx, target_idx)
                    }
                    _ => return Err(TechnicalError)
                };
                board.move_n_pile_to_pile(source_idx, target_idx, cards)?
            }
        };
        record.push_action_to_record(action_effect, self)
    }

    pub(crate) fn move_1_intent_from_selections(selection: &Selection, target: Selection) -> Result<Self> {
        let card_source = match selection {
            Selection::Waste => CardStackRef::Waste,
            Selection::Foundation(idx) => CardStackRef::Foundation(*idx),
            Selection::Pile { idx, cards: 1 } => CardStackRef::Pile(*idx),
            _ => return Err(TechnicalError)
        };
        let card_target = match target {
            Selection::Foundation(idx) => CardStackRef::Foundation(idx),
            Selection::Pile { idx, cards: 1 } => CardStackRef::Pile(idx),
            _ => return Err(TechnicalError)
        };
        Ok(Intent::Move1 {
            card_source,
            card_target,
        })
    }
}

