use serde::Deserialize;
use serde::Serialize;

use crate::board::Board;
use crate::error::Result;
use crate::move_checks::CardStack;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum CardStackRef {
    Foundation(usize),
    Pile(usize),
    Waste,
}

impl CardStackRef {
    pub(crate) fn stack<'a>(&'a self, board: &'a Board) -> Result<Box<dyn CardStack + 'a>> {
        Ok(match self {
            CardStackRef::Foundation(idx) => Box::new(board.get_foundations_by_idx(*idx)?),
            CardStackRef::Pile(idx) => Box::new(board.get_pile_by_idx(*idx)?),
            CardStackRef::Waste => Box::new(board.waste()),
        })
    }
}
