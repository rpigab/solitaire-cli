use log;

use crate::action::intent::Intent;
use crate::action::undo_record::UndoRecord;
use crate::board::Board;
use crate::error::GameError::InvalidOperation;
use crate::error::Result;

/// Record, a stack of undoable actions whose effects have already happened
#[derive(Debug)]
pub struct RedoRecord {
    intents: Vec<Intent>,
}

impl RedoRecord {
    pub(crate) fn new() -> Self {
        Self {
            intents: Vec::new(),
        }
    }

    pub(crate) fn from_replay(replay: Vec<Intent>) -> Self {
        Self {
            intents: replay,
        }
    }

    pub(crate) fn push_intent_to_record(&mut self, move_intent: Intent) -> Result<()> {
        self.intents.push(move_intent);
        log::trace!("push_intent_to_record(redo), {}", self.intents.len());
        Ok(())
    }

    pub fn redo(&mut self, board: &mut Board, undo_record: &mut UndoRecord) -> Result<()> {
        let move_intent = self.intents.pop()
            .ok_or(InvalidOperation)?;
        move_intent.apply_from_intent(board, undo_record)?;
        Ok(())
    }

    pub(crate) fn clear(&mut self) {
        self.intents.clear();
    }
}
