use log;

use crate::action::action_effect::ActionEffect;
use crate::action::intent::Intent;
use crate::action::redo_record::RedoRecord;
use crate::board::Board;
use crate::error;
use crate::error::GameError::InvalidOperation;

#[derive(Debug)]
pub(crate) struct UndoElement {
    action: ActionEffect,
    intent: Intent,
}

impl UndoElement {
    pub(crate) fn intent(&self) -> &Intent {
        &self.intent
    }
}

/// Record, a stack of undoable actions whose effects have already happened
#[derive(Debug)]
pub struct UndoRecord {
    actions: Vec<UndoElement>,
}

impl UndoRecord {
    pub(crate) fn new() -> UndoRecord {
        UndoRecord {
            actions: Vec::new(),
        }
    }

    pub(crate) fn push_action_to_record(&mut self, action: ActionEffect, intent: Intent) -> error::Result<()> {
        self.actions.push(UndoElement { action, intent });
        log::trace!("push_action_to_record(undo), {}", self.actions.len());
        Ok(())
    }

    /// pop action from undo_record, push intent to redo_record
    pub fn undo(&mut self, board: &mut Board, redo_record: &mut RedoRecord) -> error::Result<()> {
        let UndoElement { action, intent } = self.actions.pop()
            .ok_or(InvalidOperation)?;
        action.undo(board)?;
        redo_record.push_intent_to_record(intent)?;
        Ok(())
    }

    pub(crate) fn actions(&self) -> &Vec<UndoElement> {
        &self.actions
    }

    pub(crate) fn size(&self) -> usize {
        self.actions.len()
    }
}
