use std::fmt::Debug;

use crate::action::card_stack::CardStackRef;
use crate::board::Board;
use crate::error::GameError::TechnicalError;
use crate::error::Result;

/// Undoable action effect
#[derive(Debug)]
pub(crate) enum ActionEffect {
    Draw {
        /// Number of cards actually drawn from deck
        cards_drawn: u8,
    },
    Move1 {
        source: CardStackRef,
        target: CardStackRef,
        /// Whether moving cards from a pile requires to flip the next card if hidden
        reveal_last_card: bool,
    },
    MoveNPileToPile {
        number_of_cards: usize,
        source: CardStackRef,
        target: CardStackRef,
        /// Whether moving cards from a pile requires to flip the next card if hidden
        reveal_last_card: bool,
    },
}


impl ActionEffect {
    pub(crate) fn undo(&self, board: &mut Board) -> Result<()> {
        match self {
            ActionEffect::Draw { cards_drawn} => {
                board.undo_draw(*cards_drawn)
            }
            ActionEffect::Move1 { source, target, reveal_last_card } => {
                // First hide last card of source pile if it was revealed
                if let CardStackRef::Pile(source_pile_idx) = source {
                    if *reveal_last_card {
                        board.hide_last_visible_card(*source_pile_idx)?;
                    }
                }

                // Make opposite move, ignore result (don't put it in Record)
                board.do_move_1_reverse(target, source)?;

                Ok(())
            }
            ActionEffect::MoveNPileToPile {
                number_of_cards, source, target, reveal_last_card
            } => {
                let (source_idx, target_idx) = match (source, target) {
                    (CardStackRef::Pile(source_idx), CardStackRef::Pile(target_idx)) => (source_idx, target_idx),
                    _ => return Err(TechnicalError)
                };

                // First hide last card of source pile if it was revealed
                if *reveal_last_card {
                    board.hide_last_visible_card(*source_idx)?;
                }

                // Make opposite move N, ignore result (don't put it in Record)
                board.do_move_n_pile_to_pile(target_idx, source_idx, number_of_cards)?;
                Ok(())
            }
        }
    }
}
