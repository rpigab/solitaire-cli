#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum Value {
    Ace,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
}

impl Value {
    /// Returns true if this self Value directly follows other.
    ///
    /// For example, Four follows Three, King follows Queen, etc.
    pub(crate) fn follows(self, other: Value) -> bool {
        let main = self as i8;
        let other = other as i8;
        main - other == 1
    }
}

pub(crate) static ALL_VALUES: &'static [Value] = &[
    Value::Ace,
    Value::Two,
    Value::Three,
    Value::Four,
    Value::Five,
    Value::Six,
    Value::Seven,
    Value::Eight,
    Value::Nine,
    Value::Ten,
    Value::Jack,
    Value::Queen,
    Value::King,
];


#[cfg(test)]
mod tests {
    use crate::card::value::Value::{King, Queen};

    #[test]
    fn follows_ok() {
        let res = King.follows(Queen);
        assert!(res);
    }
}
