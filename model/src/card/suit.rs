#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum Suit {
    Diamonds,
    Clubs,
    Hearts,
    Spades,
}

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub enum Color {
    Black,
    Red,
}

pub(crate) static ALL_SUITS: &'static [Suit] = &[
    Suit::Diamonds,
    Suit::Clubs,
    Suit::Hearts,
    Suit::Spades,
];

impl Suit {
    pub fn get_char(&self) -> char {
        match self {
            Suit::Diamonds => '♦',
            Suit::Clubs => '♣',
            Suit::Hearts => '♥',
            Suit::Spades => '♠',
        }
    }

    pub fn get_color(self) -> Color {
        match self {
            Suit::Diamonds => Color::Red,
            Suit::Clubs => Color::Black,
            Suit::Hearts => Color::Red,
            Suit::Spades => Color::Black,
        }
    }
}
