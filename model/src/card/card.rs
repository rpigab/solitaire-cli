use crate::card::suit::Suit;
use crate::card::value::Value;

#[derive(Debug, PartialEq, Eq)]
pub struct Card {
    value: Value,
    suit: Suit,
}

impl Card {
    pub(crate) fn new(value: Value, suit: Suit) -> Card {
        Card {
            value,
            suit,
        }
    }

    pub fn value(&self) -> Value {
        self.value
    }

    pub fn suit(&self) -> Suit {
        self.suit
    }
}
