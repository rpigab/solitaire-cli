use std::mem;

use crate::action::card_stack::CardStackRef;
use crate::error::GameError::TechnicalError;
use crate::error::Result;

/// Represents selection object.
///
/// At the beggining, nothing is selected.
///
/// * Deck can't be selected, single tap action
/// * Waste can be selected before moveing the card
/// * One card from a foundation can be selected
/// * A stack of cards can be selected from a pile
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub enum Selection {
    Nothing,
    Waste,
    Foundation(usize),
    Pile { idx: usize, cards: usize },
}

impl Selection {
    pub fn clear(&mut self) {
        self.set(Selection::Nothing);
    }

    pub fn set(&mut self, new_selection: Selection) {
        *self = new_selection;
    }

    pub fn cycle(&mut self, shifted: Option<bool>, cards_visible: usize) -> Result<()> {
        match shifted {
            Some(true) => self.cycle_reversed(cards_visible),
            Some(false) | None => self.cycle_normal(),
        }
    }

    fn cycle_normal(&mut self) -> Result<()> {
        match self {
            Selection::Pile { idx, cards } => {
                let new_sel = match *cards {
                    // last card: decrementing resets selection
                    1 => Selection::Nothing,
                    // decrement from max
                    _ => Selection::Pile { idx: *idx, cards: *cards - 1 },
                };
                let _ = mem::replace(self, new_sel);
            }
            _ => return Err(TechnicalError),
        }
        Ok(())
    }

    fn cycle_reversed(&mut self, cards_visible: usize) -> Result<()> {
        match self {
            Selection::Pile { idx, cards } => {
                let new_sel = match cards {
                    // reset selection if all cards are selected
                    num_cards if *num_cards == cards_visible => Selection::Nothing,
                    // increment from max
                    _ => Selection::Pile { idx: *idx, cards: *cards + 1 },
                };
                let _ = mem::replace(self, new_sel);
            }
            _ => return Err(TechnicalError),
        }
        Ok(())
    }

    /// Find number of cards to highlight as selected, for type of card stack target
    pub fn underline_count(&self, target: Selection) -> usize {
        return if let &Selection::Pile { idx: i, .. } = &target {
            // Target to underline is a pile, if selected, return number of cards to highlight
            match self {
                &Selection::Pile { cards: c, idx } if idx == i => c,
                _ => 0,
            }
        } else {
            if *self == target { 1 } else { 0 }
        };
    }
}

impl From<CardStackRef> for Selection {
    fn from(c: CardStackRef) -> Self {
        match c {
            CardStackRef::Foundation(i) => Selection::Foundation(i),
            CardStackRef::Pile(i) => Selection::Pile { idx: i, cards: 1 },
            CardStackRef::Waste => Selection::Waste,
        }
    }
}