use crate::replay::Replay;

/// When creating a game screen,
/// choose between three mutually exclusive possibilities
pub enum NewGameMode {
    Basic,
    WithReplay(Replay),
    WithSeed([u8; 32]),
}
