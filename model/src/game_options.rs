use serde::{Deserialize, Serialize};

#[derive(Clone)]
pub struct GameOptions {
    pub draw_mode: DrawMode,
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub enum DrawMode {
    Draw1,
    Draw3,
}

impl DrawMode {
    pub fn get_num_cards_to_draw(&self) -> u8 {
        match self {
            DrawMode::Draw1 => 1,
            DrawMode::Draw3 => 3,
        }
    }
}
