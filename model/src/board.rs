use std::fmt;

use crate::action::action_effect::ActionEffect;
use crate::action::card_stack::CardStackRef;
use crate::card::card::Card;
use crate::card::suit::Suit;
use crate::card::value::Value;
use crate::card_stack::deck::Deck;
use crate::card_stack::foundation::Foundation;
use crate::card_stack::pile::Pile;
use crate::card_stack::waste::Waste;
use crate::error::GameError::{IllegalAction, InvalidOperation, TechnicalError};
use crate::error::Result;
use crate::random::gen::Gen;
use crate::selection::Selection;

pub struct Board {
    deck: Deck,
    waste: Waste,
    foundations: [Foundation; 4],
    piles: [Pile; 7],
}

impl Board {
    /// Initialize new board, return seeded Generator
    pub(crate) fn new(seed: Option<String>) -> Result<(Self, Gen)> {
        let mut board = Self::new_board();
        let mut gen: Gen = Gen::make_gen(seed)?;
        board.init(&mut gen);
        Ok((board, gen))
    }

    /// Initialize new board, replaying current seed
    pub(crate) fn new_with_gen(gen: &mut Gen) -> Self {
        let mut board = Self::new_board();
        board.init(gen);
        board
    }

    fn new_board() -> Self {
        Self {
            deck: Deck::new(),
            waste: Waste::new(),
            foundations: [(); 4].map(|_| Foundation::new()),
            piles: [(); 7].map(|_| Pile::new()),
        }
    }

    fn init(&mut self, gen: &mut Gen) {
        // Populate cards and shuffle deck
        self.deck.init(gen);
        // Draw cards from deck to all piles according to initial game setup
        for (idx, pile) in self.piles.iter_mut().enumerate() {
            let cards = self.deck.draw_cards((idx + 1) as u8);
            pile.init(cards);
        }
    }

    // Getters

    pub fn deck(&self) -> &Deck {
        &self.deck
    }

    pub fn deck_mut(&mut self) -> &mut Deck {
        &mut self.deck
    }

    pub fn waste(&self) -> &Waste {
        &self.waste
    }

    pub fn waste_mut(&mut self) -> &mut Waste {
        &mut self.waste
    }

    pub fn foundations(&self) -> &[Foundation; 4] {
        &self.foundations
    }

    pub fn piles(&self) -> &[Pile; 7] {
        &self.piles
    }


    pub(crate) fn get_any_available_foundation(&self) -> Result<CardStackRef> {
        for (i, f) in self.foundations.iter().enumerate() {
            if f.is_empty() {
                return Ok(CardStackRef::Foundation(i));
            }
        }
        return Err(InvalidOperation);
    }

    pub(crate) fn find_foundation_by_suit(&self, suit: Suit) -> Result<CardStackRef> {
        for (i, f) in self.foundations.iter().enumerate() {
            let c = f.get_top_card();
            if let Some(c) = c {
                if c.suit() == suit {
                    return Ok(CardStackRef::Foundation(i));
                }
            }
        }
        return Err(InvalidOperation);
    }

    /// Find a valid spot to put `card` in a pile,
    /// except in `except_pile_idx` if provided.
    pub(crate) fn find_spot_in_any_pile_for_card(&self, card: &Card, except_pile_idx: Option<usize>) -> Result<CardStackRef> {
        for (i, p) in self.piles.iter().enumerate()
            .filter(|(idx, _)| match except_pile_idx {
                None => true,
                Some(except_idx) => *idx != except_idx,
            }) {
            let c = p.get_first_visible_card();
            match c {
                Some(c) => {
                    // Card is next in value and of opposite color, valid
                    if c.value().follows(card.value())
                        && c.suit().get_color() != card.suit().get_color() {
                        return Ok(CardStackRef::Pile(i));
                    }
                }
                None => {
                    // On an empty spot, any King can go
                    if card.value() == Value::King {
                        return Ok(CardStackRef::Pile(i));
                    }
                }
            }
        }
        return Err(InvalidOperation);
    }

    pub(crate) fn get_main_selected_card_from_pile(&self, pile_idx: usize, num_cards_selected: usize) -> &Card {
        self.piles.get(pile_idx).unwrap().get_main_selected_card(num_cards_selected)
    }

    pub(crate) fn get_first_visible_card_from_pile(&self, pile_idx: usize) -> Option<&Card> {
        self.piles.get(pile_idx).unwrap().get_first_visible_card()
    }

    pub fn can_select(&self, sel: &Selection) -> Result<bool> {
        Ok(match sel {
            Selection::Waste => !self.waste.is_empty(),
            Selection::Foundation(idx) => !self.foundations.get(*idx)
                .ok_or(TechnicalError)?.is_empty(),
            Selection::Pile { idx, .. } => self.piles.get(*idx)
                .ok_or(TechnicalError)?.has_visible_cards(),
            _ => false,
        })
    }

    pub fn get_foundations_by_idx(&self, idx: usize) -> Result<&Foundation> {
        self.foundations.get(idx).ok_or(TechnicalError)
    }

    pub fn get_foundations_mut_by_idx(&mut self, idx: usize) -> Result<&mut Foundation> {
        self.foundations.get_mut(idx).ok_or(TechnicalError)
    }

    pub(crate) fn get_pile_by_idx(&self, idx: usize) -> Result<&Pile> {
        self.piles.get(idx).ok_or(TechnicalError)
    }

    pub fn get_pile_number_visible_by_idx(&self, idx: usize) -> usize {
        self.piles()[idx].get_size_visible()
    }

    /// Draw cards from the deck and put it on the waste.
    /// Return number of cards actually drawn.
    /// Return (cards_drawn, cards_visible_on_waste)
    pub(crate) fn draw(&mut self, cards: u8) -> u8 {
        let cards_drawn = self.deck.draw_cards(cards);
        let number_cards_drawn = cards_drawn.len() as u8;

        if cards_drawn.is_empty() {
            self.refill_deck();
        } else {
            self.waste.put_cards(cards_drawn);
        }
        number_cards_drawn
    }

    /// Undo draw
    pub(crate) fn undo_draw(&mut self, cards_drawn: u8) -> Result<()> {
        if !self.waste.is_empty() {
            // take last card(s) from waste and put them back on top of the deck
            let cards = self.waste.undo_draw(cards_drawn);
            self.deck.put_cards(cards);
        } else {
            // Waste is empty: fill waste with deck contents
            if self.deck.is_empty() {
                return Err(TechnicalError);
            }
            self.waste.fill(self.deck.drain());
        }

        Ok(())
    }

    /// Refill deck from waste
    fn refill_deck(&mut self) {
        log::debug!("refill deck");
        if !self.waste.is_empty() {
            self.deck.fill(self.waste.drain());
        }
    }

    pub(crate) fn hide_last_visible_card(&mut self, pile_idx: usize) -> Result<()> {
        self.piles.get_mut(pile_idx).ok_or(TechnicalError)?.hide_last_visible_card()?;
        Ok(())
    }

    pub(crate) fn move_1(&mut self, source: &CardStackRef, target: &CardStackRef) -> Result<ActionEffect> {
        if !self.can_move_1(&source, &target)? {
            return Err(TechnicalError);
        }
        self.do_move_1(source, target)
    }

    pub(crate) fn can_move_1(&mut self, source: &CardStackRef, target: &CardStackRef) -> Result<bool> {
        // peek source and check move
        let binding = source.stack(self)?;

        target.stack(self)?.can_put_card(binding.peek_top_card()?)
    }

    pub(crate) fn do_move_1(&mut self, source: &CardStackRef, target: &CardStackRef) -> Result<ActionEffect> {
        log::debug!("do_move_1(&mut self, source: {source:?}, target: {target:?})");
        // take actual card
        let (card, reveal_last_card) = self.take_top_card(source)?;
        // put card
        self.put_card(target, card)?;

        let source = (*source).clone();
        let target = (*target).clone();
        Ok(ActionEffect::Move1 { source, target, reveal_last_card })
    }

    pub(crate) fn do_move_1_reverse(&mut self, source: &CardStackRef, target: &CardStackRef) -> Result<()> {
        // take actual card
        let (card, _) = self.take_top_card(source)?;
        // put card
        self.put_card(target, card)
    }

    pub(crate) fn move_n_pile_to_pile(&mut self, source_idx: &usize, target_idx: &usize, cards: &usize)
                                      -> Result<ActionEffect> {
        if !self.can_move_n_pile_to_pile(source_idx, target_idx, cards)? {
            return Err(IllegalAction);
        }
        self.do_move_n_pile_to_pile(source_idx, target_idx, cards)
    }

    pub(crate) fn can_move_n_pile_to_pile(&self, source_idx: &usize, target_idx: &usize, cards: &usize)
                                          -> Result<bool> {
        let source = self.piles.get(*source_idx).ok_or(TechnicalError)?;
        let target = self.piles.get(*target_idx).ok_or(TechnicalError)?;
        let card_peek = source.get_nth_visible_card(*cards)?;

        target.check_can_put(card_peek)
    }

    pub(crate) fn do_move_n_pile_to_pile(&mut self, source_idx: &usize, target_idx: &usize, cards: &usize)
                                         -> Result<ActionEffect> {
        // Take cards (last n cards)
        // Check
        let (cards_taken, revealed_last_card) = self.piles
            .get_mut(*source_idx).ok_or(TechnicalError)?
            .take_stack(*cards)?;
        // Put cards on target pile
        self.piles.get_mut(*target_idx).ok_or(TechnicalError)?
            .put_cards(cards_taken)?;

        Ok(ActionEffect::MoveNPileToPile {
            number_of_cards: *cards,
            source: CardStackRef::Pile(*source_idx),
            target: CardStackRef::Pile(*target_idx),
            reveal_last_card: revealed_last_card,
        })
    }

    fn take_top_card(&mut self, source: &CardStackRef) -> Result<(Card, bool)> {
        match source {
            CardStackRef::Foundation(idx) => self.foundations[*idx].take_top_card(),
            CardStackRef::Pile(idx) => self.piles[*idx].take_top_card(),
            CardStackRef::Waste => self.waste.take_top_card(),
        }
    }

    fn put_card(&mut self, target: &CardStackRef, card: Card) -> Result<()> {
        match target {
            CardStackRef::Foundation(idx) => self.foundations[*idx].put_card(card),
            CardStackRef::Pile(idx) => self.piles[*idx].put_card(card),
            CardStackRef::Waste => self.waste.put_card(card),
        }
    }
}

impl fmt::Debug for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let foundations = self.foundations.iter()
            .map(|f| format!("{} cards", f.size()))
            .collect::<Vec<String>>()
            .join(", ");
        let piles = self.piles.iter()
            .map(|c| format!("a pile with {} cards", c.size()))
            .collect::<Vec<String>>()
            .join(", ");
        write!(f, "In this Board, there is:\n\t{:?},\n\t{:?},\n\t\
        4 foundations:\n\t\t{},\n\t\
        7 piles:\n\t\t{}",
               self.deck, self.waste, foundations, piles)
    }
}
