/// Pad string with spaces, centering in in a fixed size String
///
/// ```
/// use utils::display::center_str;
/// assert_eq!(center_str("hello".to_string(), 8), " hello  ".to_string());
/// assert_eq!(center_str("hello".to_string(), 3), "hel".to_string());
/// ```
pub fn center_str(mut s: String, len: usize) -> String {
    if len < s.len() {
        s.truncate(len);
        return s;
    }
    let diff = (len as i8) - (s.len() as i8);
    let div = (diff / 2, diff % 2);
    format!("{}{}{}", " ".repeat(div.0 as usize), s, " ".repeat((div.0 + div.1) as usize))
}
