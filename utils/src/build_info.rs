pub fn build_info() -> String {
    format!("Build date {} - git commit {}",
            env!("VERGEN_BUILD_TIMESTAMP"),
            env!("VERGEN_GIT_SHA_SHORT"),
    )
}
