use vergen::{Config, ShaKind, vergen};

fn main() {
    let mut config = Config::default();
    *config.git_mut().sha_kind_mut() = ShaKind::Short;
    *config.git_mut().commit_timestamp_mut() = true;
    vergen(config).expect("Error init vergen in build");
}
