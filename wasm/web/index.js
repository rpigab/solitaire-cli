//import { Terminal } from 'xterm';

const baseTheme = {
    foreground: '#F8F8F8',
    background: '#2D2E2C',
    selection: '#5DA5D533',
    black: '#1E1E1D',
    brightBlack: '#262625',
    red: '#CE5C5C',
    brightRed: '#FF7272',
    green: '#5BCC5B',
    brightGreen: '#72FF72',
    yellow: '#CCCC5B',
    brightYellow: '#FFFF72',
    blue: '#5D5DD3',
    brightBlue: '#7279FF',
    magenta: '#BC5ED1',
    brightMagenta: '#E572FF',
    cyan: '#5DA5D5',
    brightCyan: '#72F0FF',
    white: '#F8F8F8',
    brightWhite: '#FFFFFF'
};

const terminal = new Terminal(
    {
        cols: 55,
        theme: baseTheme,
        fontSize: 20
    }
);
terminal.open(document.getElementById("terminal"));
terminal.focus();

import App                      from "./app.js";
import init, { WasmController } from "./build/solitaire_cli_wasm.js";

let wasm;
init()
    .then(_wasm => {
        wasm = _wasm;
        const wasmController = WasmController.new();
        const app = new App(wasmController, terminal);
        app.run();
    })
    .catch(error => terminal.writeln(error.message));

function handleClick() {
    terminal.focus();
}

document.body.addEventListener('click', handleClick);
