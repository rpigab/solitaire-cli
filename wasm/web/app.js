
export default class App {
  constructor(wasm_controller, terminal) {
    this.wasm_controller = wasm_controller;
    this.terminal = terminal;
    this.terminal.write('\x1b[?25l');
    this.input_key_handler();
  }

  input_key_handler() {
    this.terminal.onKey(e => {
        // console.log(e.domEvent);
        let domEventLim = ((
            { code, key, altKey, ctrlKey, shiftKey }
        ) => (
            { code, key, altKey, ctrlKey, shiftKey }
        ))(e.domEvent);
        // console.log(domEventLim);
        this.wasm_controller.action(domEventLim);
        this.render();
    });
  }

  render() {
    this.terminal.write(this.wasm_controller.render());
  }

  run() {
    this.wasm_controller.init();
    this.render();
  }

}
