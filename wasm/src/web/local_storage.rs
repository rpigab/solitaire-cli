use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = localStorage)]
    pub fn setItem(key: &str, value: &str);

    #[wasm_bindgen(js_namespace = localStorage)]
    pub fn getItem(key: &str) -> Option<String>;
}
