use cli::keyboard_action::Key;
use model::error::GameError::TechnicalErrorMessage;
use model::error::Result;

use crate::wasm_controller::DomEvent;

pub(crate) fn key_to_keyboard_event(dom_event: DomEvent) -> Result<Key> {
    log::debug!("key_to_keyboard_event(): dom_event: {dom_event:#?}");

    let DomEvent {
        key, code, alt_key, ctrl_key, shift_key
    } = dom_event;

    fn extract_char(key: String) -> Result<char> {
        key.chars().nth(0)
            .map(|c| c.to_ascii_lowercase())
            .ok_or(TechnicalErrorMessage { message: "char not found in dom event".to_string() })
    }

    let key_res = match (key, code.as_str()) {
        (_, "F1") => Key::F(1),
        (_, "F2") => Key::F(2),
        (_, "F3") => Key::F(3),
        (_, "F4") => Key::F(4),
        (_, "F5") => Key::F(5),
        (_, "F6") => Key::F(6),
        (_, "F7") => Key::F(7),
        (_, "F8") => Key::F(8),
        (_, "F9") => Key::F(9),
        (_, "F10") => Key::F(10),
        (_, "F11") => Key::F(11),
        (_, "F12") => Key::F(12),
        (_, "Tab") => Key::Tab,
        (_, "Enter") => Key::Enter,
        (_, "Esc") => Key::Esc,
        (key, _) if ctrl_key => Key::Ctrl(extract_char(key)?),
        (key, _) if alt_key => Key::Alt(extract_char(key)?),
        (key, _) if shift_key => Key::Shift(extract_char(key)?),
        (key, _) => Key::Char(extract_char(key)?),
    };

    Ok(key_res)
}
