use serde::Deserialize;
use wasm_bindgen::prelude::*;

use cli::abstract_controller::Controller;
use cli::display::ansi::HIDE_CURSOR;
use cli::screens::screen_manager::ScreenManager;
use utils::build_info;

use crate::wasm_event::key_to_keyboard_event;
use crate::wasm_saver_loader::WasmSaverLoader;
use model::error::Result;

use crate::web::local_storage;

#[wasm_bindgen]
pub struct WasmController {
    screen_manager: ScreenManager,
}

impl Controller for WasmController {
    fn new() -> Result<Self> {
        init_logs();

        log::info!("{}", build_info::build_info());
        Ok(Self {
            screen_manager: ScreenManager::new(Box::new(WasmSaverLoader {})),
        })
    }
}

fn init_logs() {
    let log_level = get_log_level_localstorage();
    match log_level {
        Some(level) => console_log::init_with_level(level)
            .expect("Error during logging init"),
        _ => {}
    }
}

/// Enable logging in debug builds
/// by manually creating a local storage key with desired log level.
/// By default, logs are disabled.
fn get_log_level_localstorage() -> Option<log::Level> {
    let res = match local_storage::getItem("LOG_LEVEL")?
        .to_ascii_uppercase().as_str() {
        "TRACE" => log::Level::Trace,
        "DEBUG" => log::Level::Debug,
        "INFO" => log::Level::Info,
        "WARN" => log::Level::Warn,
        "ERROR" => log::Level::Error,
        _ => return None
    };
    Some(res)
}

#[derive(Debug, Deserialize)]
pub(crate) struct DomEvent {
    pub(crate) code: String,
    pub(crate) key: String,
    #[serde(rename(deserialize = "altKey"))]
    pub(crate) alt_key: bool,
    #[serde(rename(deserialize = "ctrlKey"))]
    pub(crate) ctrl_key: bool,
    #[serde(rename(deserialize = "shiftKey"))]
    pub(crate) shift_key: bool,
}

#[wasm_bindgen]
impl WasmController {
    pub fn new() -> WasmController {
        Controller::new().unwrap()
    }

    /// todo remove useless function called from js
    pub fn init(&mut self) {}

    pub fn action(&mut self, dom_even_js: JsValue) {
        let dom_event: DomEvent = serde_wasm_bindgen::from_value(dom_even_js).unwrap();
        let event = key_to_keyboard_event(dom_event).expect("error reading kb event");

        if let Err(e) = self.screen_manager.process_event(event) {
            log::error!("error during process_action: {:?}", e);
        }
    }

    pub fn hide_cursor(&self) -> String {
        format!("{}", HIDE_CURSOR)
    }

    pub fn render(&self) -> String {
        format!("{}", self.screen_manager)
    }
}
