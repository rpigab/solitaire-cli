use cli::saver_loader::SaverLoader;
use model::error::GameError::InvalidOperationMessage;
use model::error::Result;
use model::replay::Replay;

use crate::web::local_storage;

pub(crate) struct WasmSaverLoader {}

const REPLAY_KEY: &str = "REPLAY";

impl SaverLoader for WasmSaverLoader {
    fn save_replay(&self, replay: Replay) -> Result<()> {
        log::info!("save_replay");
        let replay_string = replay.to_cbor_base64()?;
        local_storage::setItem(REPLAY_KEY, &replay_string);
        Ok(())
    }

    fn load_replay(&self) -> Result<Replay> {
        log::info!("load_replay");
        let replay_string = local_storage::getItem(REPLAY_KEY)
            .ok_or(InvalidOperationMessage { message: format!("No replay found") })?;
        let replay = Replay::from_cbor_base64(replay_string)?;
        log::info!("replay loaded: {replay}");
        Ok(replay)
    }
}
