#!/bin/bash

cd "$(dirname "$0")"

# cargo build
cargo build --release --target wasm32-unknown-unknown
wasm-pack build --target web --out-dir ./web/build --no-typescript

# NPM
nvs auto
npm install --prefix ./web

# serve files in ./web over http and open ./web/index.html in browser
