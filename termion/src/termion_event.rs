use termion::input::TermRead;

use cli::keyboard_action::Key;
use model::error::GameError::TechnicalErrorMessage;
use model::error::Result;

pub fn get_event() -> Result<Key> {
    let key_pressed = std::io::stdin().keys().next().unwrap().unwrap();
    log::debug!("key pressed: {:?}", key_pressed);

    termion_key_to_abstract_key(key_pressed)
}

fn termion_key_to_abstract_key(key: termion::event::Key) -> Result<Key> {
    let key = match key {
        termion::event::Key::Backspace => Key::Backspace,
        termion::event::Key::Left => Key::Left,
        termion::event::Key::Right => Key::Right,
        termion::event::Key::Up => Key::Up,
        termion::event::Key::Down => Key::Down,
        termion::event::Key::Home => Key::Home,
        termion::event::Key::End => Key::End,
        termion::event::Key::PageUp => Key::PageUp,
        termion::event::Key::PageDown => Key::PageDown,
        termion::event::Key::BackTab => Key::BackTab,
        termion::event::Key::Delete => Key::Delete,
        termion::event::Key::Insert => Key::Insert,
        termion::event::Key::Esc => Key::Esc,
        termion::event::Key::F(num) => Key::F(num),
        termion::event::Key::Char('\t') => Key::Tab,
        termion::event::Key::Char('\n') => Key::Enter,
        termion::event::Key::Char(char) => Key::Char(char),
        termion::event::Key::Alt(char) => Key::Alt(char),
        termion::event::Key::Ctrl(char) => Key::Ctrl(char),
        _ => {
            return Err(TechnicalErrorMessage { message: format!("unmapped key: {:?}", key) });
        }
    };

    get_key_char_lowercased(key)
}


/// Transforms the char inside Key enum into lowercase.
fn get_key_char_lowercased(key: Key) -> Result<Key> {
    fn to_lowercase_shifted(char: char) -> (char, bool) {
        let char_lowercase = char.to_ascii_lowercase();
        (char_lowercase, char != char_lowercase)
    }

    let key = match key {
        Key::Char(char) => {
            let (char, shifted) = to_lowercase_shifted(char);
            if shifted { Key::Shift(char) } else { Key::Char(char) }
        }
        Key::Shift(char) => {
            let (char, _shifted) = to_lowercase_shifted(char);
            Key::Shift(char)
        }
        Key::Alt(char) => {
            let (char, _shifted) = to_lowercase_shifted(char);
            Key::Alt(char)
        }
        Key::Ctrl(char) => {
            let (char, _shifted) = to_lowercase_shifted(char);
            Key::Ctrl(char)
        }
        other => other,
    };

    Ok(key)
}

#[cfg(test)]
mod tests {
    use cli::keyboard_action::Key;

    use crate::termion_event::get_key_char_lowercased;

    #[test]
    fn get_key_char_lowercased_ok() {
        assert_eq!(get_key_char_lowercased(Key::Char('A')).unwrap(), Key::Shift('a'));
        assert_eq!(get_key_char_lowercased(Key::Char('w')).unwrap(), Key::Char('w'));
    }
}
