use std::io::{Stdout, stdout};
use std::io::Write;

use termion::raw::{IntoRawMode, RawTerminal};
use cli::abstract_controller::Controller;

use cli::display::ansi::{CLEAR_SCREEN, HIDE_CURSOR};
use cli::keyboard_action::Key;
use cli::screens::screen_manager::ScreenManager;
use model::error::Result;

use crate::termion_event;
use crate::termion_saver_loader::TermionSaverLoader;

pub struct TermionController {
    screen_manager: ScreenManager,
    stdout: RawTerminal<Stdout>,
}

impl Controller for TermionController {
    fn new() -> Result<Self> {
        Ok(TermionController {
            screen_manager: ScreenManager::new(Box::new(TermionSaverLoader {})),
            stdout: stdout().into_raw_mode().unwrap(),
        })
    }
}

impl TermionController {
    /// return true if game is to be exited
    pub fn wait_for_event(&mut self) -> Result<bool> {
        self.process_event(termion_event::get_event()?)
    }

    fn process_event(&mut self, event: Key) -> Result<bool> {
        match self.screen_manager.process_event(event) {
            Err(e) => {
                log::error!("error during process_action: {:?}", e);
                Ok(false)
            }
            ok => ok
        }
    }

    pub fn clean(&mut self) -> Result<()> {
        writeln!(self.stdout, "{}", CLEAR_SCREEN).unwrap();
        Ok(())
    }

    pub fn hide_cursor(&mut self) {
        writeln!(self.stdout, "{}", HIDE_CURSOR).unwrap();
    }

    pub fn render(&mut self) {
        writeln!(self.stdout, "{}", self.screen_manager).unwrap();
    }
}
