use std::fs;
use std::fs::File;
use std::io::Write;

use cli::saver_loader::SaverLoader;
use model::error::GameError::TechnicalErrorMessage;
use model::error::Result;
use model::replay::Replay;

pub(crate) struct TermionSaverLoader {}

const _REPLAY_JSON_FILE_PATH: &str = "replay.json";
const REPLAY_CBOR_FILE_PATH: &str = "replay.cbor.b64";

impl SaverLoader for TermionSaverLoader {
    fn save_replay(&self, replay: Replay) -> Result<()> {
        log::info!("saving replay");
        let replay_string = replay.to_cbor_base64()?;
        let mut file = File::create(REPLAY_CBOR_FILE_PATH)
            .map_err(|_| TechnicalErrorMessage { message: "error creating file".to_string() })?;
        file.write_all(replay_string.as_ref())
            .map_err(|_| TechnicalErrorMessage { message: "error writing to file".to_string() })?;
        Ok(())
    }

    fn load_replay(&self) -> Result<Replay> {
        log::info!("loading replay");
        let replay_string = fs::read_to_string(REPLAY_CBOR_FILE_PATH)
            .map_err(|_| TechnicalErrorMessage {
                message: format!("error reading replay from file {}", REPLAY_CBOR_FILE_PATH)
            })?;
        Replay::from_cbor_base64(replay_string)
    }
}
