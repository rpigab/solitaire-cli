#!/bin/bash

cd "$(dirname "$0")"

RUST_BACKTRACE=1 cargo -q run -F fixedseed -F logging -- "$@" 2>./log/stderr.log
