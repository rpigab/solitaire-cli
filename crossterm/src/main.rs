mod controller;
mod event;
mod saver_loader;

use std::borrow::Borrow;

use log;
#[cfg(feature = "logging")]
use log4rs;

use cli::abstract_controller::Controller;
use model::error::Result;
use utils::build_info;
use crate::controller::CrosstermController;


pub fn main() {
    #[cfg(feature = "logging")]
        let _ = log4rs::init_file("log4rs.yaml", Default::default())
        .expect("Error initializing logging");

    log::info!("{}", build_info::build_info());
    {
        let _unused = cli::keyboard_mapping::KEYBOARD_MAPPING.borrow();
    }

    if let Err(e) = manage_game() {
        log::error!("Error during game: {}", e);
    }
}

fn manage_game() -> Result<()> {
    let mut controller = CrosstermController::new()?;
    play(&mut controller)?;
    controller.clean()?;

    Ok(())
}

pub fn play(controller: &mut CrosstermController) -> Result<()> {
    while {
        controller.clean()?;
        controller.render()?;
        CrosstermController::hide_cursor()?;
        !controller.wait_for_event()?
    } {}
    Ok(())
}
