use std::io::stdout;

use crossterm::cursor::{Hide, Show};
use crossterm::execute;
use crossterm::style::Print;
use crossterm::terminal::{Clear, ClearType, disable_raw_mode, enable_raw_mode};

use cli::abstract_controller::Controller;
use cli::keyboard_action::Key;
use cli::screens::screen_manager::ScreenManager;
use model::error::GameError::TechnicalError;
use model::error::Result;

use crate::saver_loader::CrosstermSaverLoader;

pub struct CrosstermController {
    screen_manager: ScreenManager,
}

impl Controller for CrosstermController {
    fn new() -> Result<Self> {
        enable_raw_mode().unwrap();
        Ok(CrosstermController {
            screen_manager: ScreenManager::new(Box::new(CrosstermSaverLoader {})),
        })
    }
}

impl Drop for CrosstermController {
    fn drop(&mut self) {
        log::debug!("show cursor");
        CrosstermController::show_cursor().unwrap();
        log::debug!("disable raw mode");
        disable_raw_mode().unwrap();
    }
}

impl CrosstermController {
    /// return true if game is to be exited
    pub fn wait_for_event(&mut self) -> Result<bool> {
        self.process_event(crate::event::get_event()?)
    }

    fn process_event(&mut self, event: Key) -> Result<bool> {
        match self.screen_manager.process_event(event) {
            Err(e) => {
                log::error!("error during process_action: {:?}", e);
                Ok(false)
            }
            ok => ok
        }
    }

    pub fn clean(&mut self) -> Result<()> {
        execute!(stdout(), Clear(ClearType::All))
            .map_err(|_| TechnicalError)
    }

    pub fn render(&mut self) -> Result<()> {
        execute!(stdout(), Print(self.screen_manager.to_string()))
            .map_err(|_| TechnicalError)
    }

    pub fn hide_cursor() -> Result<()> {
        execute!(stdout(), Hide)
            .map_err(|_| TechnicalError)
    }

    pub fn show_cursor() -> Result<()> {
        execute!(stdout(), Show)
            .map_err(|_| TechnicalError)
    }
}
