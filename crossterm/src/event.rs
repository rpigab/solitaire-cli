use crossterm::event::{Event, KeyCode, KeyEvent, KeyModifiers};

use cli::keyboard_action::Key;
use model::error::GameError::TechnicalErrorMessage;
use model::error::Result;

pub fn get_event() -> Result<Key> {
    let event = crossterm::event::read().unwrap();

    match event {
        Event::Key(key) => {
            log::debug!("key pressed: {:?}", key);
            crossterm_key_to_abstract_key(key)
        }
        other => {
            log::debug!("other event: {other:?}");
            return Err(TechnicalErrorMessage { message: format!("other event") });
        }
    }
}

fn crossterm_key_to_abstract_key(key_event: KeyEvent) -> Result<Key> {
    log::debug!("{key_event:?}");

    let KeyEvent { code, modifiers, kind, state } = key_event;
    let key = match (code, modifiers, kind, state) {
        (KeyCode::Backspace, _, _, _) => Key::Backspace,
        (KeyCode::Left, _, _, _) => Key::Left,
        (KeyCode::Right, _, _, _) => Key::Right,
        (KeyCode::Up, _, _, _) => Key::Up,
        (KeyCode::Down, _, _, _) => Key::Down,
        (KeyCode::Home, _, _, _) => Key::Home,
        (KeyCode::End, _, _, _) => Key::End,
        (KeyCode::PageUp, _, _, _) => Key::PageUp,
        (KeyCode::PageDown, _, _, _) => Key::PageDown,
        (KeyCode::BackTab, _, _, _) => Key::BackTab,
        (KeyCode::Delete, _, _, _) => Key::Delete,
        (KeyCode::Insert, _, _, _) => Key::Insert,
        (KeyCode::Esc, _, _, _) => Key::Esc,
        (KeyCode::F(char), _, _, _) => Key::F(char),
        (KeyCode::Tab, _, _, _) => Key::Tab,
        (KeyCode::Enter, _, _, _) => Key::Enter,
        (KeyCode::Char(char), KeyModifiers::ALT, _, _) => Key::Alt(char),
        (KeyCode::Char(char), KeyModifiers::CONTROL, _, _) => Key::Ctrl(char),
        (KeyCode::Char(char), _, _, _) => Key::Char(char),
        (_, _, _, _) => {
            return Err(TechnicalErrorMessage { message: format!("unmapped key: {:?}", key_event) });
        }
    };

    get_key_char_lowercased(key)
}


/// Transforms the char inside Key enum into lowercase.
fn get_key_char_lowercased(key: Key) -> Result<Key> {
    fn to_lowercase_shifted(char: char) -> (char, bool) {
        let char_lowercase = char.to_ascii_lowercase();
        (char_lowercase, char != char_lowercase)
    }

    let key = match key {
        Key::Char(char) => {
            let (char, shifted) = to_lowercase_shifted(char);
            if shifted { Key::Shift(char) } else { Key::Char(char) }
        }
        Key::Shift(char) => {
            let (char, _shifted) = to_lowercase_shifted(char);
            Key::Shift(char)
        }
        Key::Alt(char) => {
            let (char, _shifted) = to_lowercase_shifted(char);
            Key::Alt(char)
        }
        Key::Ctrl(char) => {
            let (char, _shifted) = to_lowercase_shifted(char);
            Key::Ctrl(char)
        }
        other => other,
    };

    Ok(key)
}

#[cfg(test)]
mod tests {
    use cli::keyboard_action::Key;

    use crate::event::get_key_char_lowercased;

    #[test]
    fn get_key_char_lowercased_ok() {
        assert_eq!(get_key_char_lowercased(Key::Char('A')).unwrap(), Key::Shift('a'));
        assert_eq!(get_key_char_lowercased(Key::Char('w')).unwrap(), Key::Char('w'));
    }
}
