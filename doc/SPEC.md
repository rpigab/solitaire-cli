# Specifications

## Lexicon

- Tableau, piles, deck or stock, waste
- Fanned, upturned, downturned
- ♠ Spades
- ♣ Clubs
- ♥ Hearts
- ♦ Diamonds

## View

### Controller

Using the keyboard to select cards from their positions.

AZERTY keyboard reference, need to make it abstract for compatibility with QWERTY and others:

```
A Z E R T Y U
 Q S D F G H J
  W x c v b n
```

Press F1 for key bindings

#### Display selection

```
[ J ♠ ] [-----] [-----] [-----] [-----] [-----] [-----]
        [ 8 ♠ ] [-----] [-----] [-----] [-----] [-----]
                [ 5 ♥ ] [-----] [-----] [10 ♥ ] [-----]
                        [ 3 ♠ ] [-----] [ 9 ♠ ] [-----]
                                [ 2 ♠ ]<[ 8 ♥ ]>[-----]
                                       <[ 7 ♠ ]>[-----]
                                                [ 8 ♦ ]
```

Selected cards are underlined.

Here, cards 7 to 8 are selected on the 6th pile, after pressing twice on "H".
