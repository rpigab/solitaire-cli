# TODO

## Ideas ready to be applied

- Feat: Ensure random game is winnable -> use autoplay robot, run in parallel
- Feat: When secondary victory is achieved, use "smart" button to autoplay
    - for now, this exits the game as a normal win
- Feat: Calculate and display Score
- Feat: Change key bindings (yaml conf?)
