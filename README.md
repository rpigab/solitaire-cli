# Solitaire

Klondike Solitaire game playable in terminals, native or web terminal in a browser (using `Xterm.js`).

## How to play

### Termion version in a terminal

```shell
./termion/run.sh
```

### WebAssembly version

```shell
./wasm/build.sh
```

Open [wasm/web/index.html](wasm/web/index.html) in a browser

## Development

### Specifications

See [doc/SPEC.md](doc/SPEC.md).

## License

This project is licensed under the terms of the MIT license.

See [LICENSE.txt](./LICENSE.txt) for details.
