#!/usr/bin/env bash

set -eu

curl -sL --header "PRIVATE-TOKEN: $GLTOKEN" \
  "https://git.pierregab.net/api/v4/projects/94/packages/generic/solitaire-cli/$1/solitaire-cli-wasm.tar.gz" \
  | tar -xvz -C html --strip-components=1
