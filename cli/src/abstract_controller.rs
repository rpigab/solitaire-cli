use model::error::Result;

pub trait Controller {
    fn new() -> Result<Self> where Self: Sized;
}
