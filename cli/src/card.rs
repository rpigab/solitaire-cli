use model::card::card::Card;
use model::card::suit::Color::Red;

use crate::display::ansi;
use crate::display::show::Show;

pub(crate) const FLIPPED_CARD: &str = "[-----]";
pub(crate) const EMPTY_SPOT: &str = " · . · ";
pub const VOID_SPOT: &str = "       ";

impl Show for Card {
    fn show(&self) -> String {
        let mut res = format!("[{} {} ]", self.value().show(), self.suit().show());
        if self.suit().get_color().eq(&Red) {
            res = ansi::color_red(res);
        }
        res
    }
}
