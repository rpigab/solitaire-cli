use std::fmt::{Display, Formatter};

use model::error::GameError::{IllegalAction, InvalidOperation, TechnicalError, TechnicalErrorMessage};
use model::error::Result;
use model::game_options::{DrawMode, GameOptions};
use model::game_over::GameOver;
use model::model::Model;
use model::new_game_mode::NewGameMode;
use model::replay::Replay;
use model::selection::Selection;
use model::selection::Selection::{Foundation, Nothing, Pile, Waste};

use crate::command::Command;
use crate::coords::coords;
use crate::display::ansi::{CLEAR_SCREEN, cursor_goto, underline};
use crate::display::show::{Show, ShowStack};
use crate::keyboard_mapping::KEYBOARD_MAPPING;
use crate::message_area::MessageArea;
use crate::screens::screen::{Screen, ScreenResult, ScreenResultOk};

pub const WIDTH_COLUMNS: u16 = 55;


pub struct GameScreen {
    model: Model,
    selection: Selection,
    message_area: MessageArea,
    game_over: GameOver,
    draw_area_offsets: DrawAreaOffsets,
    /// Whether a prompt for confirmation is currently active,
    /// enabling the Confirm key to trigger it
    prompt_active: Option<Command>,
}

struct DrawAreaOffsets {
    message_area_line: u16,
    deck_waste_foundations_first_line: u16,
    piles_first_line: u16,
}

impl DrawAreaOffsets {
    fn from_draw_mode(draw_mode: &DrawMode) -> Self {
        let offset = draw_mode.get_num_cards_to_draw();

        Self {
            message_area_line: 0,
            deck_waste_foundations_first_line: offset as u16,
            piles_first_line: (2 + offset) as u16,
        }
    }
}

impl GameScreen {

    /// New GameScreen
    /// - New game
    /// - New game with replay: load seed from replay and redo stack
    /// - New game with seed: load seed only from arg
    pub(crate) fn new(mode: NewGameMode, game_options: GameOptions) -> Result<Self> {
        let draw_mode = if let NewGameMode::WithReplay(replay) = &mode {
            replay.draw_mode
        } else {
            game_options.draw_mode
        };

        Ok(Self {
            model: Model::new(mode, draw_mode)?,
            selection: Nothing,
            message_area: MessageArea::new(),
            game_over: GameOver::No,
            draw_area_offsets: DrawAreaOffsets::from_draw_mode(&draw_mode),
            prompt_active: None,
        })
    }

    fn new_game(&mut self, game_options: GameOptions) -> Result<()> {
        let new_game = Self::new(NewGameMode::Basic, game_options)?;
        let _ = std::mem::replace(self, new_game);
        Ok(())
    }

    fn new_game_with_seed(&mut self, game_options: GameOptions, seed: [u8; 32]) -> Result<()> {
        let new_game = Self::new(NewGameMode::WithSeed(seed), game_options)?;
        let _ = std::mem::replace(self, new_game);
        Ok(())
    }

    pub(crate) fn get_replay(&self) -> Result<Replay> {
        self.model.get_replay()
    }

    fn display_message_at_coords(&self, c: (u16, u16)) -> String {
        format!("{}{}", cursor_goto(c.0, c.1), self.message_area.display_message())
    }

    fn display_stack_at_coords(&self, stack: Vec<String>, c: (u16, u16), underline_count: usize) -> String {
        let res = stack.iter().enumerate()
            .map(|(i, elt)| {
                let mut elt = elt.clone();
                let c2 = coords((c.0, c.1 + i as u16));
                if i >= stack.len() - underline_count {
                    elt = underline(elt);
                }
                format!("{}{}", cursor_goto(c2.0, c2.1), elt)
            }).collect::<Vec<String>>().join("");
        format!("{}{}", res, cursor_goto(1, 1))
    }

    fn dispatch_select_or_move(&mut self, target: Selection, shifted: Option<bool>) -> Result<()> {
        match (&self.selection, target) {
            (Nothing, any) => self.select_stack(any, shifted)?,
            (_, Nothing) => self.selection.clear(),
            (_, Waste) => return Err(InvalidOperation),
            (Pile { idx: i, cards }, Pile { idx: j, .. })
            if *i != j => {
                self.model.move_n_pile_to_pile(*i, j, *cards)?;
                self.selection.clear();
            }
            (Pile { .. }, Pile { .. }) => {
                let cards_visible = self.get_max_selection_size_for_pile(&self.selection)?;
                self.selection.cycle(shifted, cards_visible)?;
            }
            (Foundation(..), Foundation(..)) => return Err(InvalidOperation),
            (_, target) => {
                self.model.move_1(&mut self.selection, target)?;
                self.selection.clear();
            }
        }
        Ok(())
    }

    fn select_stack(&mut self, sel: Selection, shifted: Option<bool>) -> Result<()> {
        if self.model.board().can_select(&sel)? {
            if let Pile { idx, .. } = sel {
                self.selection.set(Pile {
                    idx,
                    cards: {
                        match shifted {
                            Some(false) => self.model.board().get_pile_number_visible_by_idx(idx),
                            Some(true) | None => 1,
                        }
                    },
                });
            } else {
                self.selection.set(sel);
            }
        }
        Ok(())
    }

    /// Get how many visible cards are available for selection
    fn get_max_selection_size_for_pile(&self, sel: &Selection) -> Result<usize> {
        if !self.model.board().can_select(&sel)? {
            return Err(TechnicalError);
        }
        if let Pile { idx, .. } = sel {
            return Ok(self.model.board().get_pile_number_visible_by_idx(*idx));
        }
        Err(TechnicalError)
    }

    pub fn check_victory(&mut self) {
        let game_over = self.model.check_victory();
        match game_over {
            GameOver::SecondaryVictory |
            GameOver::PrimaryVictory => {
                self.message_area.edit_message(
                    format!("Game won in {} moves", self.model.get_num_moves()));
            }
            GameOver::No => {}
        }
        self.game_over = game_over;
    }

    fn prompt_before_action(&mut self, command_to_confirm: Command) -> Result<()> {
        if !self.prompt_active.is_some() {
            self.prompt_active = Some(command_to_confirm);
            self.message_area.edit_message(
                format!("Press {} to confirm",
                        KEYBOARD_MAPPING.get_key_by_cmd(&Command::Confirm)?
                )
            );
        }
        Ok(())
    }

    fn confirm_prompt_trigger_action(&mut self) -> Result<()> {
        match self.prompt_active {
            None => {
                return Err(TechnicalErrorMessage {
                    message: format!("confirm_prompt_trigger_action() called without a command")
                });
            }
            Some(Command::NewGame) => {
                self.new_game(GameOptions {
                    draw_mode: self.model.draw_mode().clone(),
                })?;
            }
            Some(Command::ReplayCurrentSeed) => {
                self.new_game_with_seed(GameOptions {
                    draw_mode: self.model.draw_mode().clone(),
                }, self.model.seed()?)?;
            }
            _ => {}
        }

        self.prompt_active = None;
        Ok(())
    }

    fn process_action(&mut self, command: Command) -> ScreenResult {
        self.message_area.clear_message();

        // First check "menu" or control type actions
        match command {
            Command::Exit => return Ok(ScreenResultOk::Close),
            Command::NewGame => {
                self.prompt_before_action(Command::NewGame)?;
                return Ok(ScreenResultOk::Nothing);
            }
            Command::ReplayCurrentSeed => {
                self.prompt_before_action(Command::ReplayCurrentSeed)?;
                return Ok(ScreenResultOk::Nothing)
            }
            Command::Confirm => {
                self.confirm_prompt_trigger_action()?;
                return Ok(ScreenResultOk::Nothing);
            }
            Command::Help => return Ok(ScreenResultOk::Help),
            Command::SaveReplay => {
                let replay = self.get_replay()?;
                return Ok(ScreenResultOk::SaveReplayAndClose(replay));
            }
            _ => {
                // Do nothing, game commands are checked after game over check
            }
        }

        // Don't check for other actions if game is already won
        if matches!(self.game_over, GameOver::PrimaryVictory) {
            return Err(IllegalAction);
        }

        // Check gameplay actions
        match command {
            Command::Draw => {
                self.selection.clear();
                self.model.draw(self.model.draw_mode().get_num_cards_to_draw())?;
            }
            Command::Select { selection, shifted } => self.dispatch_select_or_move(selection, shifted)?,
            Command::SmartMove => {
                if self.model.smart_move(&self.selection) == Ok(()) {
                    self.selection.clear();
                }
            }
            Command::Undo => {
                self.selection.clear();
                self.model.undo()?
            }
            Command::Redo => {
                self.selection.clear();
                self.model.redo()?
            }
            _ => {
                log::warn!("Board does not use command: {:?}", command);
                return Ok(ScreenResultOk::Nothing);
            }
        }

        self.check_victory();
        Ok(ScreenResultOk::Nothing)
    }
}


impl Screen for GameScreen {
    fn process_action(&mut self, command: Command) -> ScreenResult {
        let res = GameScreen::process_action(self, command);
        self.message_area.apply_updates();
        res
    }
}


impl Display for GameScreen {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        // Clear screen
        let mut res = format!("{}", CLEAR_SCREEN);

        // Message area
        res += &*self.display_message_at_coords((0, self.draw_area_offsets.message_area_line));

        // Deck
        res += &*self.display_stack_at_coords(vec![self.model.board().deck().show()],
                                              (0, self.draw_area_offsets.deck_waste_foundations_first_line), 0);
        // Waste
        let waste = self.model.board().waste().show(self.model.draw_mode());
        let waste_first_line = (1 + self.draw_area_offsets.deck_waste_foundations_first_line).checked_sub(waste.len() as u16).unwrap_or_else(|| {
            log::error!("Attempted to subtract with overflow (waste_first_line) in {}:{}:{}", file!(), line!(), column!());
            0
        });
        res += &*self.display_stack_at_coords(waste,
                                              (1, waste_first_line),
                                              self.selection.underline_count(Waste));
        // Empty
        // res += &*self.display_stack_at_coords(vec![VOID_SPOT.into()], (2, self.draw_area_offsets.deck_foundations_first_line), 0);

        for i in 0..7 {
            let u = self.selection.underline_count(Pile { idx: i, cards: 0 });
            let stack = self.model.board().piles().get(i).unwrap().show(self.model.draw_mode());
            res += &*self.display_stack_at_coords(stack, (i as u16, self.draw_area_offsets.piles_first_line), u);
        }

        for i in 0..4 {
            let u = self.selection.underline_count(Foundation(i));
            let stack = self.model.board().foundations().get(i).unwrap().show();
            res += &*self.display_stack_at_coords(vec![stack],
                                                  (i as u16 + 3, self.draw_area_offsets.deck_waste_foundations_first_line), u);
        }

        write!(f, "{}", res)
    }
}
