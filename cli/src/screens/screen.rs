use std::fmt::Display;

use model::error::GameError;
use model::game_options::DrawMode;
use model::replay::Replay;

use crate::command::Command;
use crate::keyboard::KeyboardLayout;

pub enum ScreenResultOk {
    Close,
    SaveReplayAndClose(Replay),
    NewGame,
    NewGameLoadReplay,
    Help,
    Nothing,
    SwitchKeyboardLayout(KeyboardLayout),
    SwitchDrawMode(DrawMode),
}

pub type ScreenResult = Result<ScreenResultOk, GameError>;

pub trait Screen: Display {
    fn process_action(&mut self, command: Command) -> ScreenResult;
}

impl dyn Screen {
    pub fn render(&self) -> String {
        format!("{}", self)
    }
}

impl<T: Screen + ?Sized> Screen for Box<T> {
    fn process_action(&mut self, command: Command) -> ScreenResult {
        T::process_action(self, command)
    }
}
