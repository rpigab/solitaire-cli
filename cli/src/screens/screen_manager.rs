use std::fmt::{Display, Formatter};

use model::error::Result;
use model::game_options::{DrawMode, GameOptions};
use model::new_game_mode::NewGameMode;

use crate::game_screen::GameScreen;
use crate::help_screen::HelpScreen;
use crate::keyboard::KeyboardLayout;
use crate::keyboard::KeyboardLayout::Qwerty;
use crate::keyboard_action::Key;
use crate::menu_screen::MenuScreen;
use crate::saver_loader::SaverLoader;
use crate::screens::screen::{Screen, ScreenResultOk};

/// Screen Manager handles a stack of screens, processes events,
/// turns them into actions,
/// and sends actions to the screen at the top of the stack
/// then this screen is rendered, or navigation changes the current screen
pub struct ScreenManager {
    /// Stack of screens
    screens: Vec<Box<dyn Screen>>,
    saver_loader: Box<dyn SaverLoader>,
    keyboard_layout: KeyboardLayout,
    game_options: GameOptions,
}


impl ScreenManager {
    pub fn new(saver_loader: Box<dyn SaverLoader>) -> Self {
        let screen_manager = Self {
            screens: vec![Box::new(MenuScreen::new(Qwerty, GameOptions {
                draw_mode: DrawMode::Draw1,
            }))],
            saver_loader,
            keyboard_layout: Qwerty,
            game_options: GameOptions { draw_mode: DrawMode::Draw1 },
        };
        screen_manager.hide_cursor();
        screen_manager
    }

    /// Return Ok(true) if game is to be exited
    pub fn process_event(&mut self, event: Key) -> Result<bool> {
        let command = event.to_command(&self.keyboard_layout)?;
        let last_screen = self.screens.last_mut().unwrap();

        match Screen::process_action(last_screen, command)? {
            ScreenResultOk::Close => self.close_screen(),
            ScreenResultOk::SaveReplayAndClose(replay) => {
                self.saver_loader.save_replay(replay)?;
                self.close_screen()
            }
            ScreenResultOk::NewGame => {
                self.screens.push(Box::new(
                    GameScreen::new(NewGameMode::Basic,
                                    self.game_options.clone())?));
                Ok(false)
            }
            ScreenResultOk::NewGameLoadReplay => {
                let replay = self.saver_loader.load_replay()?;
                let board = GameScreen::new(NewGameMode::WithReplay(replay),
                                            self.game_options.clone())?;
                self.screens.push(Box::new(board));
                Ok(false)
            }
            ScreenResultOk::Nothing => Ok(false),
            ScreenResultOk::Help => {
                self.screens.push(Box::new(HelpScreen { keyboard_layout: self.keyboard_layout }));
                Ok(false)
            }
            ScreenResultOk::SwitchKeyboardLayout(new_keyboard_layout) => {
                self.keyboard_layout = new_keyboard_layout;
                Ok(false)
            }
            ScreenResultOk::SwitchDrawMode(new_draw_mode) => {
                self.game_options.draw_mode = new_draw_mode;
                Ok(false)
            }
        }
    }

    fn close_screen(&mut self) -> Result<bool> {
        if self.screens.len() > 1 {
            match self.pop_screen() {
                None => log::error!("Tried to pop last screen without success"),
                Some(_) => log::info!("Popped screen")
            }
            // Close game if no screens are left
            Ok(self.screens.is_empty())
        } else {
            Ok(true)
        }
    }

    pub fn push_screen(&mut self, screen: Box<dyn Screen>) {
        self.screens.push(screen)
    }

    pub fn pop_screen(&mut self) -> Option<Box<dyn Screen>> {
        self.screens.pop()
    }

    fn hide_cursor(&self) {}
}


impl Display for ScreenManager {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", <dyn Screen>::render(self.screens.last().unwrap()))
    }
}
