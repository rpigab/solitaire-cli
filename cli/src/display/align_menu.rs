use std::fmt;
use crate::display::ansi::goto_col;

const WIDTH: usize = 55;
const SEPARATOR_IDX: usize = 14;
const SEP_CHAR: char = ' ';

pub(crate) struct Menu {
    pub(crate) title: String,
    pub(crate) items: Vec<MenuItem>,
}

/// A menu item, key and label
pub struct MenuItem(
    pub(crate) String,
    pub(crate) String,
);

impl fmt::Display for MenuItem {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f,
               "|{:>separator_idx$} {SEP_CHAR} {:<label_width$}|",
               self.0, self.1,
               separator_idx = SEPARATOR_IDX, label_width = WIDTH - SEPARATOR_IDX - 5)
    }
}

impl fmt::Display for Menu {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let cr = goto_col(0) + "\n";

        write!(f, "{:->width$}{cr}", "", width = WIDTH)?;
        write!(f, "|{: ^width$}|{cr}", self.title, width = WIDTH - 2)?;
        write!(f, "{:->width$}{cr}", "", width = WIDTH)?;
        write!(f, "|{: >width$}|{cr}", "", width = WIDTH - 2)?;

        for item in &self.items {
            write!(f, "{}{cr}", item)?;
        }

        write!(f, "|{: >width$}|{cr}", "", width = WIDTH - 2)?;
        write!(f, "{:->width$}{cr}", "", width = WIDTH)
    }
}
