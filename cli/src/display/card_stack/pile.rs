use std::iter::repeat;

use model::card::card::Card;
use model::card_stack::pile::Pile;
use model::game_options::DrawMode;

use crate::card::FLIPPED_CARD;
use crate::display::show::{Show, ShowStack};

impl ShowStack for Pile {
    fn show(&self, _draw_mode: &DrawMode) -> Vec<String> {
        // Show hidden cards and visible ones
        // need number of hidden, and read access to visible cards
        let (hidden_cards, visible_cards) = self.view_pile();
        repeat(FLIPPED_CARD.into())
            .take(hidden_cards)
            .chain(visible_cards.iter().map(Card::show))
            .collect::<Vec<String>>()
    }
}
