use model::card::card::Card;
use model::card_stack::waste::Waste;
use model::game_options::DrawMode;

use crate::card::EMPTY_SPOT;
use crate::display::show::{Show, ShowStack};

impl ShowStack for Waste {
    fn show(&self, draw_mode: &DrawMode) -> Vec<String> {
        let cards = self.get_visible_cards(draw_mode);

        if cards.is_empty() {
            vec![EMPTY_SPOT.into()]
        } else {
            cards.iter()
                .map(Card::show)
                .collect()
        }
    }
}
