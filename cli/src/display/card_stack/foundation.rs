use model::card_stack::foundation::Foundation;

use crate::card::EMPTY_SPOT;
use crate::display::show::Show;

impl Show for Foundation {
    fn show(&self) -> String {
        match self.get_top_card() {
            None => EMPTY_SPOT.into(),
            Some(card) => card.show(),
        }
    }
}
