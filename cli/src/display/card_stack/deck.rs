use model::card_stack::deck::Deck;

use crate::card::{EMPTY_SPOT, FLIPPED_CARD};
use crate::display::show::Show;

impl Show for Deck {
    fn show(&self) -> String {
        match self.is_empty() {
            false => FLIPPED_CARD.into(),
            true => EMPTY_SPOT.into(),
        }
    }
}
