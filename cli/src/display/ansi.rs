const START_UNDERLINE: &str = "\x1b[4m";
const END_UNDERLINE: &str = "\x1b[24m";

const START_RED: &str = "\x1b[31m";
const END_RED: &str = "\x1b[0m";

pub const CLEAR_SCREEN: &str = "\x1bc";

pub const HIDE_CURSOR: &str = "\x1b[?25l";

/// Cursor Goto l, c
pub fn cursor_goto(line: u16, column: u16) -> String {
    format!("\x1b[{};{}H", column, line)
}

pub fn goto_col(col: u16) -> String {
    format!("\x1b[{}G", col)
}

pub fn center_text(s: String, width_columns: u16) -> String {
    format!("{}{}", goto_col((width_columns - s.len() as u16) / 2), s)
}

/// Add underline
pub fn underline(s: String) -> String {
    format!("{}{}{}", START_UNDERLINE, s, END_UNDERLINE)
}

/// Set text to color
pub fn color_red(s: String) -> String {
    format!("{}{}{}", START_RED, s, END_RED)
}
