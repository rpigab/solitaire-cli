use model::game_options::DrawMode;

/// Allows simple elements (cards, deck) to be displayed as a string.
pub trait Show {
    fn show(&self) -> String;
}

/// Allows all board elements (stacks of cards, elements displayed across multiple lines)
/// to provide a way to be displayed as strings
/// in a column from top to bottom.
pub trait ShowStack {
    fn show(&self, draw_mode: &DrawMode) -> Vec<String>;
}
