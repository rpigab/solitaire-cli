use model::card::value::Value;

use crate::display::show::Show;

impl Show for Value {
    fn show(&self) -> String {
        match self {
            Value::Ace => " A",
            Value::Two => " 2",
            Value::Three => " 3",
            Value::Four => " 4",
            Value::Five => " 5",
            Value::Six => " 6",
            Value::Seven => " 7",
            Value::Eight => " 8",
            Value::Nine => " 9",
            Value::Ten => "10",
            Value::Jack => " J",
            Value::Queen => " Q",
            Value::King => " K",
        }.into()
    }
}
