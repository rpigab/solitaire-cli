use model::card::suit::Suit;

use crate::display::show::Show;

impl Show for Suit {
    fn show(&self) -> String {
        self.get_char().to_string()
    }
}
