use crate::game_screen::WIDTH_COLUMNS;

/// Line of text showing a message to the user, on top of the board
pub(crate) struct MessageArea {
    text: String,
    message_update: Option<MessageUpdate>,
}

#[derive(Debug)]
enum MessageUpdate {
    CleanMessage,
    EditMessage(String),
}


impl MessageArea {
    pub(crate) fn new() -> Self {
        Self {
            text: String::new(),
            message_update: None,
        }
    }

    pub(crate) fn display_message(&self) -> &String {
        &self.text
    }

    fn edit_message_priv(&mut self, mut message: String) {
        message.truncate(WIDTH_COLUMNS as usize);
        self.text = message;
    }

    pub(crate) fn edit_message(&mut self, message: String) {
        self.message_update = Some(MessageUpdate::EditMessage(message))
    }

    pub(crate) fn clear_message(&mut self) {
        self.message_update = Some(MessageUpdate::CleanMessage)
    }

    pub(crate) fn apply_updates(&mut self) {
        let update: Option<MessageUpdate> = None;
        let update = std::mem::replace(&mut self.message_update, update);
        log::debug!("apply_updates(), message_update: {update:?}");

        match update {
            Some(MessageUpdate::CleanMessage) => self.text = String::new(),
            Some(MessageUpdate::EditMessage(new_message)) => self.edit_message_priv(new_message),
            None => {}
        }

        self.message_update = None;
    }
}
