use std::fmt::{Display, Formatter};

use model::selection::Selection;
use utils::display;

use crate::command::{Command, commands_to_key_bindings_transposed};
use crate::display::ansi::CLEAR_SCREEN;
use crate::keyboard::KeyboardLayout;
use crate::screens::screen::{Screen, ScreenResult};
use crate::screens::screen::ScreenResultOk::{Close, Nothing};

pub(crate) struct HelpScreen {
    pub(crate) keyboard_layout: KeyboardLayout,
}

impl Display for HelpScreen {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let help_page_tpl = include_str!("help-page.txt").to_string();

        let commands = [
            Command::Draw,
            Command::Select { selection: Selection::Waste, shifted: None },
            Command::Select { selection: Selection::Foundation(0), shifted: None },
            Command::Select { selection: Selection::Foundation(1), shifted: None },
            Command::Select { selection: Selection::Foundation(2), shifted: None },
            Command::Select { selection: Selection::Foundation(3), shifted: None },
            Command::Select { selection: Selection::Pile { idx: 0, cards: 1 }, shifted: Some(false) },
            Command::Select { selection: Selection::Pile { idx: 1, cards: 1 }, shifted: Some(false) },
            Command::Select { selection: Selection::Pile { idx: 2, cards: 1 }, shifted: Some(false) },
            Command::Select { selection: Selection::Pile { idx: 3, cards: 1 }, shifted: Some(false) },
            Command::Select { selection: Selection::Pile { idx: 4, cards: 1 }, shifted: Some(false) },
            Command::Select { selection: Selection::Pile { idx: 5, cards: 1 }, shifted: Some(false) },
            Command::Select { selection: Selection::Pile { idx: 6, cards: 1 }, shifted: Some(false) },
            Command::Undo,
            Command::Redo,
            Command::Select { selection: Selection::Nothing, shifted: None },
            Command::SmartMove,
            Command::NewGame,
            Command::ReplayCurrentSeed,
            Command::Help,
            Command::Exit,
            Command::SaveReplay,
            Command::LoadReplay,
        ];
        let help_page = commands_to_key_bindings_transposed(&commands, &self.keyboard_layout);
        let help_page = match help_page {
            Ok(res) => res,
            Err(_) => {
                return Err(core::fmt::Error);
            }
        };
        //write!(f, "{}", center_str(out, 7))
        let help_page = help_page.iter()
            .map(|key| format!("{key}"))
            .map(|key_str| display::center_str(key_str, 7))
            .fold(help_page_tpl, |acc, x| acc.replacen("{xxxxx}", &*x, 1));

        write!(f, "{}\n{}", CLEAR_SCREEN, help_page)
    }
}

impl Screen for HelpScreen {
    fn process_action(&mut self, command: Command) -> ScreenResult {
        match command {
            Command::Exit => Ok(Close),
            _ => Ok(Nothing),
        }
    }
}
