use model::error::Result;
use model::replay::Replay;

pub trait SaverLoader {
    fn save_replay(&self, replay: Replay) -> Result<()>;

    fn load_replay(&self) -> Result<Replay>;
}
