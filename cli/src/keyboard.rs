#[derive(Copy, Clone, PartialEq, Debug)]
pub enum KeyboardLayout {
    Qwerty,
    Azerty,
}

const AZERTY_ARRAY: &str = r#"&é"'(-è_çà)=
azertyuiop^$
qsdfghjklm*
<wxcvbn,;:!"#;
const QWERTY_ARRAY: &str = r#"1234567890-=
qwertyuiop[]
asdfghjkl;\
\zxcvbnm,./"#;

impl KeyboardLayout {
    pub(crate) fn get_array(&self) -> &str {
        match self {
            KeyboardLayout::Qwerty => QWERTY_ARRAY,
            KeyboardLayout::Azerty => AZERTY_ARRAY,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::keyboard::{AZERTY_ARRAY, QWERTY_ARRAY};

    #[test]
    fn check_arrays() {
        assert_eq!(AZERTY_ARRAY.chars().count(),
                   QWERTY_ARRAY.chars().count());
    }
}