use std::fmt::{Display, Formatter};

use model::game_options::{DrawMode, GameOptions};

use crate::command::Command;
use crate::display::align_menu::{Menu, MenuItem};
use crate::display::ansi::CLEAR_SCREEN;
use crate::keyboard::KeyboardLayout;
use crate::keyboard::KeyboardLayout::{Azerty, Qwerty};
use crate::keyboard_action::transpose_key;
use crate::keyboard_mapping::KEYBOARD_MAPPING;
use crate::screens::screen::{Screen, ScreenResult, ScreenResultOk};

pub struct MenuScreen {
    keyboard_layout: KeyboardLayout,
    draw_mode: DrawMode,
}

impl MenuScreen {
    pub(crate) fn new(keyboard_layout: KeyboardLayout, game_options: GameOptions) -> Self {
        Self { keyboard_layout, draw_mode: game_options.draw_mode }
    }

    fn switch_keyboard_layout(&mut self) -> KeyboardLayout {
        self.keyboard_layout = match self.keyboard_layout {
            Qwerty => Azerty,
            Azerty => Qwerty,
        };
        self.keyboard_layout
    }

    fn switch_draw_mode(&mut self) -> DrawMode {
        self.draw_mode = match self.draw_mode {
            DrawMode::Draw1 => DrawMode::Draw3,
            DrawMode::Draw3 => DrawMode::Draw1,
        };
        self.draw_mode
    }
}


impl Screen for MenuScreen {
    fn process_action(&mut self, command: Command) -> ScreenResult {
        match command {
            Command::Exit => Ok(ScreenResultOk::Close),
            Command::NewGame => Ok(ScreenResultOk::NewGame),
            Command::DrawModeSwitch => Ok(ScreenResultOk::SwitchDrawMode(self.switch_draw_mode())),
            Command::LoadReplay => Ok(ScreenResultOk::NewGameLoadReplay),
            Command::Help => Ok(ScreenResultOk::Help),
            Command::Switch => Ok(ScreenResultOk::SwitchKeyboardLayout(self.switch_keyboard_layout())),
            _ => Ok(ScreenResultOk::Nothing),
        }
    }
}


impl Display for MenuScreen {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let menu_items: Vec<MenuItem> = [
            (Command::NewGame, "new game".to_string()),
            (Command::DrawModeSwitch, format!("switch draw mode ({:?})", self.draw_mode)),
            (Command::Help, "open help".to_string()),
            (Command::Exit, "back/exit".to_string()),
            (Command::LoadReplay, "load replay".to_string()),
            (Command::Switch, format!("switch keyboard layout ({:?})", self.keyboard_layout)),
        ].iter()
            .map(|(cmd, label)| {
                let key = KEYBOARD_MAPPING.get_key_by_cmd(&cmd).unwrap();
                let key = transpose_key(key, &self.keyboard_layout).unwrap();
                MenuItem(key.to_string(), label.to_string())
            }).collect();

        let menu = Menu {
            title: "Solitaire CLI".to_string(),
            items: menu_items,
        };

        write!(f, "{clear}{menu}", clear = CLEAR_SCREEN)
    }
}
