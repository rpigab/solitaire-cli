use std::fmt::{Display, Formatter};

use model::error::GameError::TechnicalError;
use model::error::Result;

use crate::command::Command;
use crate::keyboard::KeyboardLayout;
use crate::keyboard::KeyboardLayout::Qwerty;
use crate::keyboard_mapping::KEYBOARD_MAPPING;

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub enum Key {
    /// Backspace.
    Backspace,
    /// Left arrow.
    Left,
    /// Right arrow.
    Right,
    /// Up arrow.
    Up,
    /// Down arrow.
    Down,
    /// Home key.
    Home,
    /// End key.
    End,
    /// Page Up key.
    PageUp,
    /// Page Down key.
    PageDown,
    /// Tab key.
    Tab,
    /// Enter key.
    Enter,
    /// Backward Tab key.
    BackTab,
    /// Delete key.
    Delete,
    /// Insert key.
    Insert,
    /// Function keys.
    ///
    /// Only function keys 1 through 12 are supported.
    F(u8),
    /// Normal character.
    Char(char),
    /// Shift character.
    Shift(char),
    /// Alt modified character.
    Alt(char),
    /// Ctrl modified character.
    ///
    /// Note that certain keys may not be modifiable with `ctrl`, due to limitations of terminals.
    Ctrl(char),
    /// Null byte.
    Null,
    /// Esc key.
    /// Try to avoid using it, as it is used for escape sequences depending on context,
    /// might cause delays.
    Esc,
}

impl Key {
    pub fn to_command(self, keyboard_layout: &KeyboardLayout) -> Result<Command> {
        log::debug!("to_command: {self:?}");

        let transposed_key = match self {
            Key::Char(char) => {
                let qwerty_char = transpose_char(char, &keyboard_layout, &Qwerty);
                let c = match qwerty_char {
                    Ok(c) => c,
                    Err(_) => char,
                };
                Key::Char(c)
            }
            Key::Shift(char) => Key::Shift(transpose_char(char, &keyboard_layout, &Qwerty)?),
            Key::Ctrl(char) => Key::Ctrl(transpose_char(char, &keyboard_layout, &Qwerty)?),
            Key::Alt(char) => Key::Alt(transpose_char(char, &keyboard_layout, &Qwerty)?),
            other => other,
        };

        log::info!("transposed_key: {transposed_key:?}");

        let command = KEYBOARD_MAPPING
            .get_cmd_by_key(&transposed_key)
            .unwrap_or_else(|_| {
                log::warn!("unbound key: {:?}", transposed_key);
                &Command::Nothing
            }).clone();

        Ok(command.clone())
    }
}

/// ```
/// use cli::keyboard::KeyboardLayout::{Azerty, Qwerty};
/// use cli::keyboard_action::transpose_char;
/// assert_eq!(transpose_char('a', &Azerty, &Qwerty).unwrap(), 'q');
/// assert_eq!(transpose_char('q', &Qwerty, &Azerty).unwrap(), 'a');
/// assert_eq!(transpose_char('m', &Qwerty, &Azerty).unwrap(), ',');
/// assert_eq!(transpose_char('m', &Azerty, &Qwerty).unwrap(), ';');
/// assert_eq!(transpose_char('1', &Qwerty, &Azerty).unwrap(), '&');
/// assert_eq!(transpose_char('2', &Qwerty, &Azerty).unwrap(), 'é');
/// assert_eq!(transpose_char('&', &Azerty, &Qwerty).unwrap(), '1');
/// assert_eq!(transpose_char('é', &Azerty, &Qwerty).unwrap(), '2');
/// assert_eq!(transpose_char(' ', &Azerty, &Qwerty).unwrap(), ' ');
/// assert_eq!(transpose_char(' ', &Qwerty, &Azerty).unwrap(), ' ');
/// ```
pub fn transpose_char(c: char, source_layout: &KeyboardLayout, target_layout: &KeyboardLayout)
                      -> Result<char> {
    let mut source_chars = source_layout.get_array().chars();
    let res = match source_chars.position(|c2| c2 == c) {
        None => c,
        Some(index) => target_layout.get_array().chars().nth(index)
            .ok_or(TechnicalError)?
    };
    Ok(res)
}

pub(crate) fn transpose_key(key: &Key, keyboard_layout: &KeyboardLayout) -> Result<Key> {
    let transposed_key = match key {
        Key::Char(c) => Key::Char(transpose_char(*c, keyboard_layout, &Qwerty)?),
        Key::Shift(c) => Key::Shift(transpose_char(*c, keyboard_layout, &Qwerty)?),
        Key::Alt(c) => Key::Alt(transpose_char(*c, keyboard_layout, &Qwerty)?),
        Key::Ctrl(c) => Key::Ctrl(transpose_char(*c, keyboard_layout, &Qwerty)?),
        other => other.clone(),
    };
    Ok(transposed_key)
}


impl Display for Key {

    /// ```
    /// use cli::keyboard_action::Key;
    /// assert_eq!(format!("{}", Key::Enter).as_str(), "Enter");
    /// assert_eq!(format!("{}", Key::Up).as_str(), "Up");
    /// ```
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let out = match self {
            Key::Backspace => "Bckspc".to_string(),
            Key::Left => "Left".to_string(),
            Key::Right => "Right".to_string(),
            Key::Up => "Up".to_string(),
            Key::Down => "Down".to_string(),
            Key::Home => "Home".to_string(),
            Key::End => "End".to_string(),
            Key::PageUp => "PgUp".to_string(),
            Key::PageDown => "PgDwn".to_string(),
            Key::Enter => "Enter".to_string(),
            Key::BackTab => "BckTab".to_string(),
            Key::Delete => "Del".to_string(),
            Key::Insert => "Ins".to_string(),
            Key::Esc => "Esc".to_string(),
            Key::Tab => "Tab".to_string(),
            Key::F(c) => format!("F{c}"),
            Key::Char(' ') => format!("Space"),
            Key::Char(c) => format!("{c}"),
            Key::Shift(c) => format!("Shf-{c}"),
            Key::Alt(c) => format!("Alt-{c}"),
            Key::Ctrl(c) => format!("Ctr-{c}"),
            _ => "".to_string(),
        };
        write!(f, "{out}")
    }
}
