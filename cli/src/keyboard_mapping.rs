use std::collections::HashMap;
use std::iter::{IntoIterator, Iterator};

use bimap::{BiHashMap, BiMap};
use once_cell::sync::Lazy;

use model::error::GameError::InvalidOperation;
use model::error::Result;
use model::selection::Selection;

use crate::command::Command;
use crate::keyboard_action::Key;

pub struct KeyboardMapping {
    /// Main key to command 2-way map
    main_key_command_bimap: BiMap<Key, Command>,
    /// Extra keys also bound to the command as the main key referenced here
    additional_keys_to_main_key_map: HashMap<Key, &'static [Key]>,
}

/// A key binding
/// - Main key
/// - Associated command
/// - Extra keys also bound to the same command
type KeyBinding = (
    Key,
    Command,
    &'static [Key],
);


pub static KEYBOARD_MAPPING: Lazy<KeyboardMapping> = Lazy::new(|| {
    KeyboardMapping::new()
});

impl KeyboardMapping {
    fn new() -> KeyboardMapping {
        let mut main_key_command_bimap = BiHashMap::new();
        let mut additional_keys_to_main_key_map = HashMap::new();

        KEY_BINDINGS.into_iter()
            .for_each(|(key, command, additional_keys)| {
                main_key_command_bimap.insert_no_overwrite(key.clone(), command).unwrap();
                additional_keys_to_main_key_map.insert(key, additional_keys);
            });

        KeyboardMapping {
            additional_keys_to_main_key_map,
            main_key_command_bimap,
        }
    }

    pub(crate) fn get_key_by_cmd(&self, command: &Command) -> Result<&Key> {
        self.main_key_command_bimap.get_by_right(command).ok_or(InvalidOperation)
    }

    pub(crate) fn get_cmd_by_key(&self, key: &Key) -> Result<&Command> {
        // if key is in additional keys, replace it with the main binding before processing
        let additional_keys_vec: Vec<&Key> = self.additional_keys_to_main_key_map.iter()
            .filter_map(|(main_key, additional_keys)| {
                additional_keys.contains(key).then_some(main_key)
            }).collect();

        let binding_key = if additional_keys_vec.len() == 1 {
            additional_keys_vec.get(0).unwrap()
        } else {
            key
        };

        // process main key
        match binding_key {
            // ignore shift for commands that don't need a variation
            Key::Shift(c) => {
                self.main_key_command_bimap.get_by_left(key)
                    .or_else(|| self.main_key_command_bimap.get_by_left(&Key::Char(*c)))
                    .ok_or(InvalidOperation)
            }
            key => self.main_key_command_bimap.get_by_left(key).ok_or(InvalidOperation),
        }
    }
}

const KEY_BINDINGS: [KeyBinding; 33] = [
    (Key::Char('q'), Command::Draw, &[]),
    (Key::Char('w'), Command::Select { selection: Selection::Waste, shifted: None }, &[]),
    (Key::Char('e'), Command::Select { selection: Selection::Nothing, shifted: None }, &[]),
    (Key::Char('r'), Command::Select { selection: Selection::Foundation(0), shifted: None }, &[]),
    (Key::Char('t'), Command::Select { selection: Selection::Foundation(1), shifted: None }, &[]),
    (Key::Char('y'), Command::Select { selection: Selection::Foundation(2), shifted: None }, &[]),
    (Key::Char('u'), Command::Select { selection: Selection::Foundation(3), shifted: None }, &[]),
    (Key::Char('a'), Command::Select { selection: Selection::Pile { idx: 0, cards: 1 }, shifted: Some(false) }, &[]),
    (Key::Char('s'), Command::Select { selection: Selection::Pile { idx: 1, cards: 1 }, shifted: Some(false) }, &[]),
    (Key::Char('d'), Command::Select { selection: Selection::Pile { idx: 2, cards: 1 }, shifted: Some(false) }, &[]),
    (Key::Char('f'), Command::Select { selection: Selection::Pile { idx: 3, cards: 1 }, shifted: Some(false) }, &[]),
    (Key::Char('g'), Command::Select { selection: Selection::Pile { idx: 4, cards: 1 }, shifted: Some(false) }, &[]),
    (Key::Char('h'), Command::Select { selection: Selection::Pile { idx: 5, cards: 1 }, shifted: Some(false) }, &[]),
    (Key::Char('j'), Command::Select { selection: Selection::Pile { idx: 6, cards: 1 }, shifted: Some(false) }, &[]),
    (Key::Shift('a'), Command::Select { selection: Selection::Pile { idx: 0, cards: 1 }, shifted: Some(true) }, &[]),
    (Key::Shift('s'), Command::Select { selection: Selection::Pile { idx: 1, cards: 1 }, shifted: Some(true) }, &[]),
    (Key::Shift('d'), Command::Select { selection: Selection::Pile { idx: 2, cards: 1 }, shifted: Some(true) }, &[]),
    (Key::Shift('f'), Command::Select { selection: Selection::Pile { idx: 3, cards: 1 }, shifted: Some(true) }, &[]),
    (Key::Shift('g'), Command::Select { selection: Selection::Pile { idx: 4, cards: 1 }, shifted: Some(true) }, &[]),
    (Key::Shift('h'), Command::Select { selection: Selection::Pile { idx: 5, cards: 1 }, shifted: Some(true) }, &[]),
    (Key::Shift('j'), Command::Select { selection: Selection::Pile { idx: 6, cards: 1 }, shifted: Some(true) }, &[]),
    (Key::F(7), Command::Undo, &[Key::Ctrl('z')]),
    (Key::F(8), Command::Redo, &[Key::Ctrl('y')]),
    (Key::Ctrl('d'), Command::Exit, &[Key::Esc]),
    (Key::Char('n'), Command::NewGame, &[]),
    (Key::Char('b'), Command::ReplayCurrentSeed, &[]),
    (Key::Char('k'), Command::DrawModeSwitch, &[]),
    (Key::F(1), Command::Help, &[]),
    (Key::F(3), Command::SaveReplay, &[]),
    (Key::F(4), Command::LoadReplay, &[]),
    (Key::Tab, Command::Switch, &[]),
    (Key::Char(' '), Command::SmartMove, &[]),
    (Key::Enter, Command::Confirm, &[]),
];

#[cfg(test)]
mod tests {
    use model::selection::Selection;

    use crate::command::Command;
    use crate::keyboard_action::Key;
    use crate::keyboard_mapping::KEYBOARD_MAPPING;

    #[test]
    fn get_cmd_by_key_ok() {
        let f = |key| KEYBOARD_MAPPING.get_cmd_by_key(&key).unwrap();

        assert_eq!(&Command::Draw, f(Key::Char('q')));
        assert_eq!(&Command::Select { selection: Selection::Pile { idx: 0, cards: 1 }, shifted: Some(false) }, f(Key::Char('a')));
        assert_eq!(&Command::Select { selection: Selection::Nothing, shifted: None }, f(Key::Char('e')));
        assert_eq!(&Command::Select { selection: Selection::Nothing, shifted: None }, f(Key::Shift('e')));
    }
}
