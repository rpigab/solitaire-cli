use model::error::Result;
use model::selection::Selection;
use crate::keyboard_mapping::KEYBOARD_MAPPING;

use crate::keyboard::KeyboardLayout;
use crate::keyboard_action::{Key, transpose_key};

/// Represents a player command, from a key press
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub enum Command {
    /// Exit the game
    Exit,
    /// New game
    NewGame,
    /// Like new game, but loads last game seed,
    /// only available if last game isn't exited
    ReplayCurrentSeed,
    /// Switch from draw1 to draw3
    DrawModeSwitch,
    /// Used to trigger effect relevant in development
    Dev(char),
    /// Draw from deck or put waste back to deck
    Draw,
    /// Select stack of cards as source or target of move.
    /// Select nothing means deselect current.
    Select {
        selection: Selection,
        /// Whether the shift key was pressed, triggering alternate use of action.
        /// Option::None if there is no alternate version.
        shifted: Option<bool>,
    },
    /// Undo last move
    Undo,
    /// Redo last Undone move
    Redo,
    /// Open help
    Help,
    /// Save current Replay to file
    SaveReplay,
    /// Load replay from file
    LoadReplay,
    /// Switch selected element
    Switch,
    /// Smart move, move selection to any valid spot, with foundation as priority
    SmartMove,
    /// Confirm action
    Confirm,
    /// Do nothing
    Nothing,
}

pub(crate) fn commands_to_key_bindings_transposed(commands: &[Command], keyboard_layout: &KeyboardLayout) -> Result<Vec<Key>> {
    commands.into_iter()
        .map(|cmd|
            KEYBOARD_MAPPING.get_key_by_cmd(&cmd).unwrap())
        .map(|src_key| transpose_key(src_key, &keyboard_layout))
        .collect::<Result<Vec<Key>>>()
}
