use crate::card::FLIPPED_CARD;

/// Transform card coordinates on board to terminal coords
pub fn coords(c: (u16, u16)) -> (u16, u16) {
    let card_length: u16 = FLIPPED_CARD.len().try_into().unwrap();
    (c.0 * (card_length + 1) + 1, c.1 + 1)
}
